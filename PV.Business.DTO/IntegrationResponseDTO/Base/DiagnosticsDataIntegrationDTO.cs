﻿using System.Collections.Generic;

namespace PV.Business.DTO.IntegrationResponseDTO.Base
{
    public class DiagnosticsDataIntegrationDTO
    {
        public bool Success { get; set; }
        public List<VictronDiagnosticDataRecord> Records { get; set; }
        public int Num_records { get; set; }
    }
}