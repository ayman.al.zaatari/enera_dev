﻿using Newtonsoft.Json;

namespace PV.Business.DTO.IntegrationResponseDTO.Base
{
    public class LoginResponseIntegrationDTO
    {
        [JsonProperty(PropertyName = "idUser")]
        public string Id { get; set; }
        public string Token { get; set; }
    }
}
