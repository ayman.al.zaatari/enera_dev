﻿using System.Collections.Generic;

namespace PV.Business.DTO.IntegrationResponseDTO.Base
{
    public class InstallationsIntegrationResponseDTO<Installation> where Installation : InstallationResponseIntegrationDTO
    {
        public List<Installation> Records { get; set; }
    }
}
