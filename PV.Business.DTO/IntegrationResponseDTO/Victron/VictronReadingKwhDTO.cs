﻿using System.Collections.Generic;

namespace PV.Business.DTO.IntegrationResponseDTO.Victron
{
    public class PairValue
    {
        public List<decimal> DecimalValue { get; set; }
    }
    public class Records
    {
        public double[][] Pc { get; set; }
        public double[][] Pb { get; set; }
        public double[][] Gb { get; set; }
        public double[][] Gc { get; set; }
        public double[][] Pg { get; set; }
        public double[][] Bc { get; set; }
        public double[][] Kwh { get; set; }
        public double[][] Bg { get; set; }

    }
    public class Totals
    {
        public decimal Pb { get; set; }
        public decimal Pc { get; set; }
        public decimal Gb { get; set; }
        public decimal Gc { get; set; }
        public decimal Pg { get; set; }
        public decimal Bc { get; set; }
        public decimal Kwh { get; set; }
        public decimal Bg { get; set; }

    }
    public class VictronReadingKwhDTO
    {
        public bool Success { get; set; }
        public Records Records { get; set; }
        public Totals Totals { get; set; }

    }
}