﻿using PV.Business.DTO.IntegrationResponseDTO.Base;
using PV.Business.DTO.IntegrationResponseDTO.Victron;

namespace PV.Business.DTO.IntegrationResponseDTO
{
    public class VictronConnectedDevicesIntegrationDTO : ConnectedDevicesIntegrationDTO
    {
        public bool Success { get; set; }
        public VictronConnectedDevicesRecordsDTO Records { get; set; }
    }
}
