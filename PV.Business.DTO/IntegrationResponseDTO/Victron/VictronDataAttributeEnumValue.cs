﻿namespace PV.Business.DTO.IntegrationResponseDTO
{
    public class VictronDataAttributeEnumValue
    {
        public string NameEnum { get; set; }
        public int ValueEnum { get; set; }
    }
}