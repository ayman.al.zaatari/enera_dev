﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace PV.Business.DTO.IntegrationResponseDTO.Victron
{
    public class VictronConnectedDevicesRecordsDTO
    {
        public List<VictronConnectedDevice> Devices { get; set; }
        [JsonProperty(PropertyName = "unconfigured_devices")]
        public bool UnconfiguredDevices { get; set; }
    }
}
