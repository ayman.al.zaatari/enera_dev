﻿using Newtonsoft.Json;
using PV.Business.DTO.IntegrationResponseDTO.Base;

namespace PV.Business.DTO.IntegrationResponseDTO
{
    public class VictronInstallationResponseIntegrationDTO : InstallationResponseIntegrationDTO
    {
        public int AccessLevel { get; set; }
        public bool Owner { get; set; }

        [JsonProperty(PropertyName = "is_admin")]
        public bool IsAdmin { get; set; }
        public string Name { get; set; }
        public string Identifier { get; set; }
        public int IdUser { get; set; }
        public string PvMax { get; set; }
        public string Timezone { get; set; }
        public string Phonenumber { get; set; }
        public object Notes { get; set; }
        public object Geofence { get; set; }
        public bool GeofenceEnabled { get; set; }
        public int HasMains { get; set; }
        public object HasGenerator { get; set; }
        public object NoDataAlarmTimeout { get; set; }
        public int AlarmMonitoring { get; set; }
        public string InvalidVRMAuthTokenUsedInLogRequest { get; set; }
        public string InvalidMqttPasswordSentAt { get; set; }
        public int Syscreated { get; set; }

        [JsonProperty(PropertyName = "device_icon")]
        public string DeviceIcon { get; set; }
    }
}
