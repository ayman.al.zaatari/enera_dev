﻿using Newtonsoft.Json;

namespace PV.Business.DTO.IntegrationResponseDTO.Victron
{
    public class VictronConnectedDevice
    {
        public string Name { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string FirmwareVersion { get; set; }
        public int LastConnection { get; set; }

        [JsonProperty(PropertyName = "@class")]
        public string Class { get; set; }
        public int LoggingInterval { get; set; }
        public int LastPowerUpOrRestart { get; set; }
        public int? Instance { get; set; }
    }
}