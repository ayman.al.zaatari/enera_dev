﻿using System.Collections.Generic;

namespace PV.Business.DTO
{
    public class DiagnosticsDataDTO
    {
        public List<DiagnosticDataDTO> List { get; set; }
    }
}
