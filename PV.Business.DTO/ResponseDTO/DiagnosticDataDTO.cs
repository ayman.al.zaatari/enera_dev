﻿using System;

namespace PV.Business.DTO
{
    public class DiagnosticDataDTO
    {
        public Guid Id { get; private set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public int IdSite { get; set; }
        public DateTimeOffset? Timestamp { get; set; }
        public string Device { get; set; }
        public int Instance { get; set; }
        public int IdDataAttribute { get; set; }
        public string Description { get; set; }
        public string FormatWithUnit { get; set; }
        public string DbusServiceType { get; set; }
        public string DbusPath { get; set; }
        public string FormattedValue { get; set; }
        public int RequestId { get; set; }
        public Guid ReadingId { get; set; }
    }
}