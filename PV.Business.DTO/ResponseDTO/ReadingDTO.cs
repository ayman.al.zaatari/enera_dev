﻿using System;

namespace PV.Business.DTO
{
    public class ReadingDTO
    {
        public Guid Id { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
    }
}
