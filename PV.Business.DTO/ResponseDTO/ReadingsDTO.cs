﻿using System.Collections.Generic;

namespace PV.Business.DTO
{
    public class ReadingsDTO
    {
        public List<ReadingDTO> Readings { get; set; }
    }
}
