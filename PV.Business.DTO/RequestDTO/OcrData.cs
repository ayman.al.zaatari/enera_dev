﻿using System;

namespace PV.Business.DTO.RequestDTO
{
    public class OcrData
    {
        public string MeterReading { get; set; }
        public DateTimeOffset SyncedOn
        {
            get
            {
                return SyncedOn;
            }
            set
            {
                SyncedOn = DateTimeOffset.UtcNow;
            }
        }
    }
}