﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PV.Business.DTO
{
    public class SupplierUpdateRequestDTO : SupplierRequestDTO
    {
        [Required]
        public Guid Id { get; set; }
    }
}
