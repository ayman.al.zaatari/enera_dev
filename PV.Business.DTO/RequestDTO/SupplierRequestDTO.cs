﻿using System.ComponentModel.DataAnnotations;

namespace PV.Business.DTO
{
    public class SupplierRequestDTO
    {
        [Required]
        public string Key { get; set; }
        [Required]
        public string Value { get; set; }
    }
}
