﻿using System.Collections.Generic;

namespace PV.Business.DTO.RequestDTO
{
    public class SyncReadingsDTO
    {
        List<byte[]> Images { get; set; }
        List<OcrData> Data { get; set; }
    }
}
