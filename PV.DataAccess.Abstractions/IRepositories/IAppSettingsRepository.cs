﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using PV.Core.Models;

namespace PV.DataAccess.Abstractions.IRepositories
{
    public interface IAppSettingsRepository : IRepository<AppSettings>
    {
        Task<AppSettings> GetByKey(string key, CancellationToken ct);
        Task<List<AppSettings>> GetByKeys(List<string> keys, CancellationToken ct);
    }
}
