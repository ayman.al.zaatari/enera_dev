﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using PV.Core.Models;

namespace PV.DataAccess.Abstractions.IRepositories
{
    public interface IRepository<T> where T : BaseModel
    {
        Task<List<T>> GetAll(CancellationToken ct, bool isDeleted = false);

        Task<T> GetAsync(Guid id, CancellationToken ct, bool isDeleted = false);

        Task<T> GetAsNoTrackingAsync(Guid id, CancellationToken ct);

        Task AddAsync(T entity, CancellationToken ct);

        Task DeleteAsync(Guid id, CancellationToken ct);

        Task UpdateAsync(T entity, CancellationToken ct);

        Task AddRangeAsync(IEnumerable<T> entities, CancellationToken ct);
    }
}
