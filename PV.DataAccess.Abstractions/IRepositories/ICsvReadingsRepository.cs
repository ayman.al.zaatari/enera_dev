﻿using System;
using System.Threading;
using System.Threading.Tasks;
using PV.Core.Models;

namespace PV.DataAccess.Abstractions.IRepositories
{
    public interface ICsvReadingRepository : IRepository<CsvReading>
    {
        Task<DateTimeOffset?> GetLatestTimeStamp(int siteId, CancellationToken ct);
    }
}
