﻿using PV.Core.Models;

namespace PV.DataAccess.Abstractions.IRepositories
{
    public interface ISupplierRepository : IRepository<Supplier>
    {
    }
}
