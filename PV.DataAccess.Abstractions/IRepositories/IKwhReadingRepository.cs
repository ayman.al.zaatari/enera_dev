﻿using PV.Core.Models;

namespace PV.DataAccess.Abstractions.IRepositories
{
    public interface IKwhReadingRepository : IRepository<KwhReading>
    {
    }
}
