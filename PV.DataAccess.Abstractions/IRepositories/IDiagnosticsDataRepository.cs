﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using PV.Core.Models;

namespace PV.DataAccess.Abstractions.IRepositories
{
    public interface IDiagnosticsDataRepository : IRepository<DiagnosticsData>
    {
        Task<List<DiagnosticsData>> GetDiagnosticsDataByReading(Guid readingId, CancellationToken ct);
    }
}
