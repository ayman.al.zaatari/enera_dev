﻿using System;
using PV.Core.Models;

namespace PV.DataAccess.Extensions
{
    public static class BaseModelExtensions
    {
        public static BaseModel SetCreatedOnUtcNow(this BaseModel baseModel)
        {
            baseModel.CreatedOn = DateTimeOffset.UtcNow;

            return baseModel;
        }
    }
}
