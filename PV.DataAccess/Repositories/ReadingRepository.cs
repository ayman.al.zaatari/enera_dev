﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PV.Core.Models;
using PV.DataAccess.Abstractions.IRepositories;
using PV.DataAccess.Context;

namespace PV.DataAccess.Repositories
{
    public class ReadingRepository : Repository<Reading>, IReadingRepository
    {
        public ReadingRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
        }

        public async Task<Reading> GetReadingWithDiagnosticsData(Guid readingId, CancellationToken ct)
        {
            var result = await DbContext.Reading.Where(x => x.Id.Equals(readingId)).Include(x => x.DiagnosticsData).FirstOrDefaultAsync(ct);

            return result;
        }
    }
}
