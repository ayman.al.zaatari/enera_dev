﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PV.Core.Models;
using PV.DataAccess.Abstractions.IRepositories;
using PV.DataAccess.Context;

namespace PV.DataAccess.Repositories
{
    public class AppSettingsRepository : Repository<AppSettings>, IAppSettingsRepository
    {
        public AppSettingsRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
            
        }

        public async Task<AppSettings> GetByKey(string key, CancellationToken ct)
        {
            var result = await DbContext.Set<AppSettings>().FirstOrDefaultAsync(x => x.Key.Equals(key), ct);

            return result;
        }

        public async Task<List<AppSettings>> GetByKeys(List<string> keys, CancellationToken ct)
        {
            var result = await DbContext.Set<AppSettings>().Where(x => keys.Contains(x.Key)).ToListAsync(ct);

            return result;
        }
    }
}
