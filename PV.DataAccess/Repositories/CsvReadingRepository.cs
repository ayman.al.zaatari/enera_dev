﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PV.Core.Models;
using PV.DataAccess.Abstractions.IRepositories;
using PV.DataAccess.Context;

namespace PV.DataAccess.Repositories
{
    public class CsvReadingRepository : Repository<CsvReading>, ICsvReadingRepository
    {
        public CsvReadingRepository(DatabaseContext databaseContext) : base(databaseContext)
        {

        }

        public Task<DateTimeOffset?> GetLatestTimeStamp(int siteId, CancellationToken ct)
        {
            var result = DbContext.Set<CsvReading>().Where(x => x.Site.IdSite.Equals(siteId)).Select(x => x.TimeStamp).OrderByDescending(x => x).FirstOrDefaultAsync(ct);

            return result;
        }
    }
}
