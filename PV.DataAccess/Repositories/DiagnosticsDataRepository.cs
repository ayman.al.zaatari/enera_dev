﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PV.Core.Models;
using PV.DataAccess.Abstractions.IRepositories;
using PV.DataAccess.Context;

namespace PV.DataAccess.Repositories
{
    public class DiagnosticsDataRepository : Repository<DiagnosticsData> , IDiagnosticsDataRepository
    {
        public DiagnosticsDataRepository(DatabaseContext databaseContext) : base(databaseContext)
        {

        }

        public async Task<List<DiagnosticsData>> GetDiagnosticsDataByReading(Guid readingId, CancellationToken ct)
        {
            var result = await DbContext.Set<DiagnosticsData>().Where(x => x.ReadingId.Equals(readingId)).ToListAsync(ct);

            return result;
        }
    }
}
