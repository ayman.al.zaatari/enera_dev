﻿using PV.Core.Models;
using PV.DataAccess.Abstractions.IRepositories;
using PV.DataAccess.Context;

namespace PV.DataAccess.Repositories
{
    public class DiagnosticsDataEnumValueRepository : Repository<DiagnosticsDataEnumValue> , IDiagnosticsDataEnumValueRepository
    {
        public DiagnosticsDataEnumValueRepository(DatabaseContext databaseContext) : base(databaseContext)
        {

        }
    }
}
