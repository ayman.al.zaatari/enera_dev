﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PV.Core.Models;
using PV.DataAccess.Abstractions.IRepositories;
using PV.DataAccess.Context;

namespace PV.DataAccess.Repositories
{
    public class Repository<T> : IRepository<T> where T : BaseModel
    {
        public DatabaseContext DbContext { get; set; }

        public Repository(DatabaseContext dbcontext)
        {
            this.DbContext = dbcontext;
        }

        public async Task<T> GetAsNoTrackingAsync(Guid id, CancellationToken ct)
        {
            var result = await DbContext.Set<T>().AsNoTracking().FirstOrDefaultAsync(e => e.Id.Equals(id) && !e.IsDeleted, ct);

            return result;
        }

        public async Task<T> GetAsync(Guid id, CancellationToken ct, bool isDeleted = false)
        {
            var result = await DbContext.Set<T>().FirstOrDefaultAsync(e => e.Id.Equals(id) && e.IsDeleted.Equals(isDeleted), ct);

            return result;
        }

        public async Task<List<T>> GetAll(CancellationToken ct, bool isDeleted = false)
        {
            var result = await DbContext.Set<T>().Where(x => x.IsDeleted.Equals(isDeleted)).ToListAsync(ct);

            return result;
        }

        public async Task AddAsync(T entity, CancellationToken ct)
        {
            entity.CreatedOn = DateTimeOffset.UtcNow;

            await DbContext.Set<T>().AddAsync(entity, ct);
        }

        public async Task DeleteAsync(Guid id, CancellationToken ct)
        {
            var entity = await GetAsync(id, ct);

            entity.IsDeleted = true;

            entity.UpdatedOn = DateTimeOffset.UtcNow;
        }

        public async Task UpdateAsync(T entity, CancellationToken ct)
        {
            //TODO: Fix the CreatedOn
            entity.UpdatedOn = DateTimeOffset.UtcNow;

            //var createdOn = DbContext.Set<T>().Attach(entity).Property(x => x.CreatedOn).OriginalValue;

            //entity.CreatedOn = createdOn;

            var previous = await GetAsync(entity.Id, ct);

            entity.CreatedOn = previous.CreatedOn;

            DbContext.Entry(entity).State = EntityState.Modified;
        }

        public async Task AddRangeAsync(IEnumerable<T> entities, CancellationToken ct)
        {
            for (int i = 0; i < entities.Count(); i++)
            {
                await AddAsync(entities.ElementAt(i), ct);
            }
        }
    }
}
