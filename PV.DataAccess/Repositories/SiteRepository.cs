﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PV.Core.Models;
using PV.DataAccess.Abstractions.IRepositories;
using PV.DataAccess.Context;

namespace PV.DataAccess.Repositories
{
    public class SiteRepository : Repository<Site>, ISiteRepository
    {
        public SiteRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
        }

        public async Task<Site> GetSiteReadingsByExternalId(int siteId, CancellationToken ct)
        {
            var result = await DbContext.Site.Where(x => x.IdSite.Equals(siteId)).Include(y => y.Readings).FirstOrDefaultAsync(ct);

            return result;
        }
    }
}
