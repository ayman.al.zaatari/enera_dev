﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PV.Core.Models;
using PV.DataAccess.Abstractions.IRepositories;
using PV.DataAccess.Context;

namespace PV.DataAccess.Repositories
{
    public class KwhReadingRepository : Repository<KwhReading>, IKwhReadingRepository
    {
        public KwhReadingRepository(DatabaseContext databaseContext) : base(databaseContext)
        {

        }
    }
}
