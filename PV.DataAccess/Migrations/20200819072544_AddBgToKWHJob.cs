﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PV.DataAccess.Migrations
{
    public partial class AddBgToKWHJob : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "BgValue",
                table: "KwhReading",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BgValue",
                table: "KwhReading");
        }
    }
}
