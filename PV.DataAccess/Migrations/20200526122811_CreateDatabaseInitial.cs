﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PV.DataAccess.Migrations
{
    public partial class CreateDatabaseInitial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppSettings",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedOn = table.Column<DateTimeOffset>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppSettings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Site",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedOn = table.Column<DateTimeOffset>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IdSite = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Site", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Suppliers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedOn = table.Column<DateTimeOffset>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Suppliers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CsvReading",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedOn = table.Column<DateTimeOffset>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    TimeStamp = table.Column<DateTimeOffset>(nullable: true),
                    Gateway0_VrmLogTimeOffset = table.Column<decimal>(nullable: true),
                    Gateway0_EssScheduledCharging = table.Column<string>(nullable: true),
                    Gateway0_Relay1State = table.Column<string>(nullable: true),
                    Gateway0_GeneratorRunReason = table.Column<string>(nullable: true),
                    Gateway0_PhaseRotation = table.Column<string>(nullable: true),
                    Gateway0_InputVoltagePhase1 = table.Column<decimal>(nullable: true),
                    Gateway0_InputVoltagePhase2 = table.Column<decimal>(nullable: true),
                    Gateway0_InputVoltagePhase3 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_InputCurrentPhase1 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_InputCurrentPhase2 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_InputCurrentPhase3 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_InputFrequency1 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_InputFrequency2 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_InputFrequency3 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_InputPower1 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_InputPower2 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_InputPower3 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_OutputVoltagePhase1 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_OutputVoltagePhase2 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_OutputVoltagePhase3 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_OutputCurrentPhase1 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_OutputCurrentPhase2 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_OutputCurrentPhase3 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_OutputFrequency = table.Column<decimal>(nullable: true),
                    VeBusSystem0_OutputPower1 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_OutputPower2 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_OutputPower3 = table.Column<decimal>(nullable: true),
                    VeBusSystem0_Voltage = table.Column<decimal>(nullable: true),
                    VeBusSystem0_Current = table.Column<decimal>(nullable: true),
                    VeBusSystem0_ActiveInput = table.Column<string>(nullable: true),
                    VeBusSystem0_ActiveInputCurrentLimit = table.Column<decimal>(nullable: false),
                    VeBusSystem0_VeBusStateOfCharge = table.Column<decimal>(nullable: false),
                    VeBusSystem0_VeBusState = table.Column<string>(nullable: true),
                    VeBusSystem0_VeBusError = table.Column<string>(nullable: true),
                    VeBusSystem0_SwitchPosition = table.Column<string>(nullable: true),
                    VeBusSystem0_Temperature = table.Column<string>(nullable: true),
                    VeBusSystem0_LowBattery = table.Column<string>(nullable: true),
                    VeBusSystem0_Overload = table.Column<string>(nullable: true),
                    VeBusSystem0_TemperaturSensorAlarm = table.Column<string>(nullable: true),
                    VeBusSystem0_VoltageSensorAlarm = table.Column<string>(nullable: true),
                    VeBusSystem0_HighDcRipple = table.Column<string>(nullable: true),
                    VeBusSystem0_TemperatureL1 = table.Column<string>(nullable: true),
                    VeBusSystem0_LowBatteryL1 = table.Column<string>(nullable: true),
                    VeBusSystem0_OverloadL1 = table.Column<string>(nullable: true),
                    VeBusSystem0_HighDcRippleL1 = table.Column<string>(nullable: true),
                    VeBusSystem0_TemperatureL2 = table.Column<string>(nullable: true),
                    VeBusSystem0_LowBatteryL2 = table.Column<string>(nullable: true),
                    VeBusSystem0_OverloadL2 = table.Column<string>(nullable: true),
                    VeBusSystem0_HighDcRippleL2 = table.Column<string>(nullable: true),
                    VeBusSystem0_TemperatureL3 = table.Column<string>(nullable: true),
                    VeBusSystem0_LowBatteryL3 = table.Column<string>(nullable: true),
                    VeBusSystem0_OverloadL3 = table.Column<string>(nullable: true),
                    VeBusSystem0_HighDcRippleL3 = table.Column<string>(nullable: true),
                    VeBusSystem0_ChargeState = table.Column<string>(nullable: true),
                    SolarCharger_Voltage = table.Column<decimal>(nullable: true),
                    SolarCharger_Current = table.Column<decimal>(nullable: true),
                    SolarCharger_BatteryWatts = table.Column<decimal>(nullable: false),
                    SolarCharger_LoadState = table.Column<string>(nullable: true),
                    SolarCharger_ChargerOnOff = table.Column<string>(nullable: true),
                    SolarCharger_ChargeState = table.Column<string>(nullable: true),
                    SolarCharger_PvVoltage = table.Column<decimal>(nullable: true),
                    SolarCharger_PvCurrent = table.Column<decimal>(nullable: true),
                    SolarCharger_PvPower = table.Column<decimal>(nullable: false),
                    SolarCharger_RelayOnTheCharger = table.Column<string>(nullable: true),
                    SolarCharger_YieldToday = table.Column<decimal>(nullable: true),
                    SolarCharger_MaximumChargePowerToday = table.Column<decimal>(nullable: false),
                    SolarCharger_YieldYesterday = table.Column<decimal>(nullable: true),
                    SolarCharger_MaximumChargePowerYesterday = table.Column<decimal>(nullable: false),
                    SolarCharger_ErrorCode = table.Column<string>(nullable: true),
                    SolarCharger_UserYield = table.Column<decimal>(nullable: true),
                    SolarCharger_Voltage2 = table.Column<decimal>(nullable: true),
                    SolarCharger_Current2 = table.Column<decimal>(nullable: true),
                    SolarCharger_BatteryWatts2 = table.Column<decimal>(nullable: false),
                    SolarCharger_LoadState2 = table.Column<string>(nullable: true),
                    SolarCharger_ChargerOnOff2 = table.Column<string>(nullable: true),
                    SolarCharger_ChargeState2 = table.Column<string>(nullable: true),
                    SolarCharger_PvVoltage2 = table.Column<decimal>(nullable: true),
                    SolarCharger_PvCurrent2 = table.Column<decimal>(nullable: true),
                    SolarCharger_PvPower2 = table.Column<decimal>(nullable: false),
                    SolarCharger_RelayOnTheCharger2 = table.Column<string>(nullable: true),
                    SolarCharger_YieldToday2 = table.Column<decimal>(nullable: true),
                    SolarCharger_MaximumChargePowerToday2 = table.Column<decimal>(nullable: false),
                    SolarCharger_YieldYesterday2 = table.Column<decimal>(nullable: true),
                    SolarCharger_MaximumChargePowerYesterday2 = table.Column<decimal>(nullable: false),
                    SolarCharger_ErrorCode2 = table.Column<string>(nullable: true),
                    SolarCharger_UserYield2 = table.Column<decimal>(nullable: true),
                    SolarCharger_Voltage3 = table.Column<decimal>(nullable: true),
                    SolarCharger_Current3 = table.Column<decimal>(nullable: true),
                    SolarCharger_BatteryWatts3 = table.Column<decimal>(nullable: false),
                    SolarCharger_LoadState3 = table.Column<string>(nullable: true),
                    SolarCharger_ChargerOnOff3 = table.Column<string>(nullable: true),
                    SolarCharger_ChargeState3 = table.Column<string>(nullable: true),
                    SolarCharger_PvVoltage3 = table.Column<decimal>(nullable: true),
                    SolarCharger_PvCurrent3 = table.Column<decimal>(nullable: true),
                    SolarCharger_PvPower3 = table.Column<decimal>(nullable: false),
                    SolarCharger_RelayOnTheCharger3 = table.Column<string>(nullable: true),
                    SolarCharger_YieldToday3 = table.Column<decimal>(nullable: true),
                    SolarCharger_MaximumChargePowerToday3 = table.Column<decimal>(nullable: false),
                    SolarCharger_YieldYesterday3 = table.Column<decimal>(nullable: true),
                    SolarCharger_MaximumChargePowerYesterday3 = table.Column<decimal>(nullable: false),
                    SolarCharger_ErrorCode3 = table.Column<string>(nullable: true),
                    SolarCharger_UserYield3 = table.Column<decimal>(nullable: true),
                    SolarCharger_Voltage4 = table.Column<decimal>(nullable: true),
                    SolarCharger_Current4 = table.Column<decimal>(nullable: true),
                    SolarCharger_BatteryWatts4 = table.Column<decimal>(nullable: false),
                    SolarCharger_LoadState4 = table.Column<string>(nullable: true),
                    SolarCharger_ChargerOnOff4 = table.Column<string>(nullable: true),
                    SolarCharger_ChargeState4 = table.Column<string>(nullable: true),
                    SolarCharger_PvVoltage4 = table.Column<decimal>(nullable: true),
                    SolarCharger_PvCurrent4 = table.Column<decimal>(nullable: true),
                    SolarCharger_PvPower4 = table.Column<decimal>(nullable: false),
                    SolarCharger_RelayOnTheCharger4 = table.Column<string>(nullable: true),
                    SolarCharger_YieldToday4 = table.Column<decimal>(nullable: true),
                    SolarCharger_MaximumChargePowerToday4 = table.Column<decimal>(nullable: false),
                    SolarCharger_YieldYesterday4 = table.Column<decimal>(nullable: true),
                    SolarCharger_MaximumChargePowerYesterday4 = table.Column<decimal>(nullable: false),
                    SolarCharger_ErrorCode4 = table.Column<string>(nullable: true),
                    SolarCharger_UserYield4 = table.Column<decimal>(nullable: true),
                    SystemOverview0_AcInput = table.Column<string>(nullable: true),
                    SystemOverview0_PvDcCoupled = table.Column<string>(nullable: true),
                    SystemOverview0_AcConsumptionL1 = table.Column<decimal>(nullable: false),
                    SystemOverview0_AcConsumptionL2 = table.Column<decimal>(nullable: false),
                    SystemOverview0_AcConsumptionL3 = table.Column<decimal>(nullable: false),
                    SystemOverview0_GridL1 = table.Column<decimal>(nullable: false),
                    SystemOverview0_GridL2 = table.Column<decimal>(nullable: false),
                    SystemOverview0_GridL3 = table.Column<decimal>(nullable: false),
                    SystemOverview0_Voltage = table.Column<decimal>(nullable: true),
                    SystemOverview0_BatteryStateOfCharge = table.Column<decimal>(nullable: true),
                    SystemOverview0_DvccMultipleBatteriesAlarm = table.Column<string>(nullable: true),
                    SystemOverview0_DvccFirmwareInsufficientAlarm = table.Column<string>(nullable: true),
                    SystemOverview0_OneLowSocDischargeDisabled = table.Column<string>(nullable: true),
                    SystemOverview0_TwoBatteryLifeIsActive = table.Column<string>(nullable: true),
                    SystemOverview0_ThreeChargeDisabledByBms = table.Column<string>(nullable: true),
                    SystemOverview0_FourDischargeDisabledByBms = table.Column<string>(nullable: true),
                    SystemOverview0_FiveSlowChargeIsActive = table.Column<string>(nullable: true),
                    SystemOverview0_SixChargeDisabledByUserSetting = table.Column<string>(nullable: true),
                    SystemOverview0_SevenDischargeDisabledByUserSetting = table.Column<string>(nullable: true),
                    SystemOverview0_Current = table.Column<decimal>(nullable: true),
                    SystemOverview0_VeBusChargeCurrent = table.Column<decimal>(nullable: true),
                    SystemOverview0_BatteryPower = table.Column<string>(nullable: true),
                    SystemOverview0_VeBusChargePower = table.Column<string>(nullable: true),
                    SystemOverview0_BatteryState = table.Column<string>(nullable: true),
                    SystemOverview0_GeneratorNotDetectedAtAcInput = table.Column<string>(nullable: true),
                    SolarCharger_MpptState = table.Column<string>(nullable: true),
                    SiteId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CsvReading", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CsvReading_Site_SiteId",
                        column: x => x.SiteId,
                        principalTable: "Site",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Reading",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedOn = table.Column<DateTimeOffset>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    SiteId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reading", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reading_Site_SiteId",
                        column: x => x.SiteId,
                        principalTable: "Site",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DiagnosticsDataHistory",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedOn = table.Column<DateTimeOffset>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IdSite = table.Column<int>(nullable: false),
                    Timestamp = table.Column<DateTimeOffset>(nullable: true),
                    Device = table.Column<string>(nullable: true),
                    Instance = table.Column<int>(nullable: false),
                    IdDataAttribute = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    FormatWithUnit = table.Column<string>(nullable: true),
                    DbusServiceType = table.Column<string>(nullable: true),
                    DbusPath = table.Column<string>(nullable: true),
                    FormattedValue = table.Column<string>(nullable: true),
                    RequestId = table.Column<int>(nullable: false),
                    ReadingId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiagnosticsDataHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DiagnosticsDataHistory_Reading_ReadingId",
                        column: x => x.ReadingId,
                        principalTable: "Reading",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DiagnosticsDataEnumValueHistory",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedOn = table.Column<DateTimeOffset>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DiagnosticsDataId = table.Column<Guid>(nullable: false),
                    NameEnum = table.Column<string>(nullable: true),
                    ValueEnum = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiagnosticsDataEnumValueHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DiagnosticsDataEnumValueHistory_DiagnosticsDataHistory_DiagnosticsDataId",
                        column: x => x.DiagnosticsDataId,
                        principalTable: "DiagnosticsDataHistory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AppSettings",
                columns: new[] { "Id", "CreatedBy", "CreatedOn", "IsDeleted", "Key", "UpdatedBy", "UpdatedOn", "Value" },
                values: new object[] { new Guid("31295e86-0b1c-4172-977e-22ffd5b055cd"), null, new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), false, "SelaataNextCsv", null, null, "1/1/2020 12:00:00 AM +00:00" });

            migrationBuilder.InsertData(
                table: "AppSettings",
                columns: new[] { "Id", "CreatedBy", "CreatedOn", "IsDeleted", "Key", "UpdatedBy", "UpdatedOn", "Value" },
                values: new object[] { new Guid("3f599270-78ef-4b91-b495-be6b2394ad4d"), null, new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), false, "FermesNextCsv", null, null, "1/1/2020 12:00:00 AM +00:00" });

            migrationBuilder.InsertData(
                table: "AppSettings",
                columns: new[] { "Id", "CreatedBy", "CreatedOn", "IsDeleted", "Key", "UpdatedBy", "UpdatedOn", "Value" },
                values: new object[] { new Guid("a644ae79-e564-40a9-8c8f-ce3ed3350e35"), null, new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), false, "RabihMNextCsv", null, null, "1/1/2020 12:00:00 AM +00:00" });

            migrationBuilder.CreateIndex(
                name: "IX_CsvReading_SiteId",
                table: "CsvReading",
                column: "SiteId");

            migrationBuilder.CreateIndex(
                name: "IX_DiagnosticsDataEnumValueHistory_DiagnosticsDataId",
                table: "DiagnosticsDataEnumValueHistory",
                column: "DiagnosticsDataId");

            migrationBuilder.CreateIndex(
                name: "IX_DiagnosticsDataHistory_ReadingId",
                table: "DiagnosticsDataHistory",
                column: "ReadingId");

            migrationBuilder.CreateIndex(
                name: "IX_Reading_SiteId",
                table: "Reading",
                column: "SiteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppSettings");

            migrationBuilder.DropTable(
                name: "CsvReading");

            migrationBuilder.DropTable(
                name: "DiagnosticsDataEnumValueHistory");

            migrationBuilder.DropTable(
                name: "Suppliers");

            migrationBuilder.DropTable(
                name: "DiagnosticsDataHistory");

            migrationBuilder.DropTable(
                name: "Reading");

            migrationBuilder.DropTable(
                name: "Site");
        }
    }
}
