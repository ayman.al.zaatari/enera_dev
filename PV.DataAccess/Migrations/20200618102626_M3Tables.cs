﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PV.DataAccess.Migrations
{
    public partial class M3Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FeederTable",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedOn = table.Column<DateTimeOffset>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    FeederID = table.Column<string>(nullable: true),
                    FeederName = table.Column<string>(nullable: true),
                    PrimarySubstation = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    IP = table.Column<string>(nullable: true),
                    AddressLow = table.Column<string>(nullable: true),
                    M3Validity = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeederTable", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "M3ReadingTable",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedOn = table.Column<DateTimeOffset>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    FeederID = table.Column<string>(nullable: true),
                    ReadingDate = table.Column<DateTimeOffset>(nullable: true),
                    DateOfGreatestCurrent = table.Column<DateTimeOffset>(nullable: true),
                    ActiveEnergy = table.Column<decimal>(nullable: false),
                    ActiveEnergyExport = table.Column<decimal>(nullable: false),
                    ReactiveEnergyImport = table.Column<decimal>(nullable: false),
                    MaxVoltage_Ph1 = table.Column<decimal>(nullable: false),
                    MaxVoltage_Ph2 = table.Column<decimal>(nullable: false),
                    MaxVoltage_Ph3 = table.Column<decimal>(nullable: false),
                    MaxCurrent_Ph1 = table.Column<decimal>(nullable: false),
                    MaxCurrent_Ph2 = table.Column<decimal>(nullable: false),
                    MaxCurrent_Ph3 = table.Column<decimal>(nullable: false),
                    GreatestCurrentReached = table.Column<decimal>(nullable: false),
                    MinVoltage_Ph1 = table.Column<decimal>(nullable: false),
                    MinVoltage_Ph2 = table.Column<decimal>(nullable: false),
                    MinVoltage_Ph3 = table.Column<decimal>(nullable: false),
                    ModifyDate = table.Column<string>(nullable: true),
                    ReptRecordNum = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M3ReadingTable", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FeederTable");

            migrationBuilder.DropTable(
                name: "M3ReadingTable");

        }
    }
}
