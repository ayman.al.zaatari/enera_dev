﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PV.DataAccess.Migrations
{
    public partial class M3LoadProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "M3LoadProfiles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedOn = table.Column<DateTimeOffset>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    FeederName = table.Column<string>(nullable: true),
                    PrimarySubStation = table.Column<string>(nullable: true),
                    MeterID = table.Column<string>(nullable: true),
                    ReferenceTimeStamp = table.Column<DateTime>(nullable: false),
                    MeterTimeStamp = table.Column<DateTime>(nullable: false),
                    TotalActiveEnergyImport = table.Column<double>(nullable: false),
                    TotalReactiveEnergyImport = table.Column<double>(nullable: false),
                    ApparentPower1 = table.Column<double>(nullable: false),
                    ApparentPower2 = table.Column<double>(nullable: false),
                    ApparentPower3 = table.Column<double>(nullable: false),
                    ActivePower1 = table.Column<double>(nullable: false),
                    ActivePower2 = table.Column<double>(nullable: false),
                    ActivePower3 = table.Column<double>(nullable: false),
                    PowerFactor1 = table.Column<decimal>(type: "decimal(18, 6)", nullable: false),
                    PowerFactor2 = table.Column<decimal>(type: "decimal(18, 6)", nullable: false),
                    PowerFactor3 = table.Column<decimal>(type: "decimal(18, 6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M3LoadProfiles", x => x.Id);
                });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "M3LoadProfiles");
        }
    }
}
