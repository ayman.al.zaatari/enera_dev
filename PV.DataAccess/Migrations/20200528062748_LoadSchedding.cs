﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PV.DataAccess.Migrations
{
    public partial class LoadSchedding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LoadSchedding",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedOn = table.Column<DateTimeOffset>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Date = table.Column<DateTimeOffset>(nullable: false),
                    Day = table.Column<string>(nullable: true),
                    ZeroToSix = table.Column<string>(nullable: true),
                    SixToTen = table.Column<string>(nullable: true),
                    TenToFourteen = table.Column<string>(nullable: true),
                    FourteenToEighteen = table.Column<string>(nullable: true),
                    EighteenToTwentyFour = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoadSchedding", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LoadSchedding");
        }
    }
}
