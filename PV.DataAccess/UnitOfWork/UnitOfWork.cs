﻿using System.Threading;
using System.Threading.Tasks;
using PV.DataAccess.Abstractions.IRepositories;
using PV.DataAccess.Abstractions.IUnitOfWork;
using PV.DataAccess.Context;

namespace PV.DataAccess.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        public DatabaseContext Context;

        private readonly IAppSettingsRepository AppSettings;

        private readonly ISupplierRepository Suppliers;

        private readonly IDiagnosticsDataRepository DiagnosticsDatas;

        private readonly IDiagnosticsDataEnumValueRepository DiagnosticsDataEnumValues;

        private readonly IReadingRepository Readings;

        private readonly ISiteRepository Sites;

        private readonly ICsvReadingRepository CsvReadings;

        private readonly IKwhReadingRepository KwhReadings;

        public UnitOfWork(DatabaseContext context,
            IAppSettingsRepository appSettings,
            IDiagnosticsDataRepository diagnosticsDatas,
            IDiagnosticsDataEnumValueRepository diagnosticsDataEnumValues,
            IReadingRepository readings,
            ICsvReadingRepository csvReadings,
            ISiteRepository sites,
            ISupplierRepository suppliers,
            IKwhReadingRepository kwhReadings)
        {
            Context = context;
            DiagnosticsDataEnumValues = diagnosticsDataEnumValues;
            DiagnosticsDatas = diagnosticsDatas;
            AppSettings = appSettings;
            Readings = readings;
            Suppliers = suppliers;
            CsvReadings = csvReadings;
            Sites = sites;
            KwhReadings = kwhReadings;
        }

        IAppSettingsRepository IUnitOfWork.AppSettings => AppSettings;

        ISupplierRepository IUnitOfWork.Suppliers => Suppliers;

        IDiagnosticsDataRepository IUnitOfWork.DiagnosticsDatas => DiagnosticsDatas;

        IDiagnosticsDataEnumValueRepository IUnitOfWork.DiagnosticsDataEnumValues => DiagnosticsDataEnumValues;

        IReadingRepository IUnitOfWork.Readings => Readings;

        ISiteRepository IUnitOfWork.Sites => Sites;

        ICsvReadingRepository IUnitOfWork.CsvReadings => CsvReadings;

        IKwhReadingRepository IUnitOfWork.KwhReadings => KwhReadings;

        public async Task SaveChangesAsync(CancellationToken ct)
        {
            await Context.SaveChangesAsync(ct);
        }
    }
}
