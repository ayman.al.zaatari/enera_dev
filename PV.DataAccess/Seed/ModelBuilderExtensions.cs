﻿using System;
using Microsoft.EntityFrameworkCore;
using PV.Core.Models;

namespace PV.DataAccess.Seed
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppSettings>().HasData(
                new AppSettings { Id = Guid.NewGuid(), Key = "SelaataNextCsv", Value = "1/1/2020 12:00:00 AM +00:00" },
                new AppSettings { Id = Guid.NewGuid(), Key = "FermesNextCsv", Value = "1/1/2020 12:00:00 AM +00:00" },
                new AppSettings { Id = Guid.NewGuid(), Key = "RabihMNextCsv", Value = "1/1/2020 12:00:00 AM +00:00" },
                new AppSettings { Id = Guid.NewGuid(), Key = "SelaataNextKwh", Value = "1/1/2020 12:00:00 AM +00:00" },
                new AppSettings { Id = Guid.NewGuid(), Key = "FermesNextKwh", Value = "1/1/2020 12:00:00 AM +00:00" },
                new AppSettings { Id = Guid.NewGuid(), Key = "RabihMNextKwh", Value = "1/1/2020 12:00:00 AM +00:00" }
            );
        }
    }
}
