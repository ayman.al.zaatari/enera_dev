﻿using Microsoft.EntityFrameworkCore;
using PV.Core.Models;
using PV.DataAccess.Seed;

namespace PV.DataAccess.Context
{
    public class DatabaseContext : DbContext
    {
        public DbSet<AppSettings> AppSettings { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<DiagnosticsData> DiagnosticsDataHistory { get; set; }
        public DbSet<DiagnosticsDataEnumValue> DiagnosticsDataEnumValueHistory { get; set; }
        public DbSet<Reading> Reading { get; set; }
        public DbSet<Site> Site { get; set; }
        public DbSet<CsvReading> CsvReading { get; set; }
        public DbSet<LoadSchedding> LoadSchedding { get; set; }
        public DbSet<KwhReading> KwhReading { get; set; }
        public DbSet<FeederTable> FeederTable { get; set; }
        public DbSet<M3ReadingTable> M3ReadingTable { get; set; }

        public DatabaseContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder ModelBuilder)
        {
            ModelBuilder.Entity<DiagnosticsData>()
                .HasMany(c => c.DataAttributeEnumValues)
                .WithOne(x => x.DiagnosticsData);

            ModelBuilder.Entity<DiagnosticsData>()
                .HasOne(c => c.Reading)
                .WithMany(x => x.DiagnosticsData);


            ModelBuilder.Entity<Reading>()
                .HasOne(c => c.Site)
                .WithMany(x => x.Readings);

            ModelBuilder.Entity<CsvReading>()
               .HasOne(c => c.Site)
               .WithMany(x => x.CsvReadings);

            ModelBuilder.Entity<KwhReading>()
            .HasOne(c => c.Site)
            .WithMany(x => x.KwhReadings);

            ModelBuilder.Entity<M3LoadProfiles>()
            .Property(x => x.PowerFactor1)
            .HasColumnType("decimal(18, 6)");

            ModelBuilder.Entity<M3LoadProfiles>()
            .Property(x => x.PowerFactor2)
            .HasColumnType("decimal(18, 6)");

            ModelBuilder.Entity<M3LoadProfiles>()
            .Property(x => x.PowerFactor3)
            .HasColumnType("decimal(18, 6)");

            ModelBuilder.Seed();
        }
    }
}
