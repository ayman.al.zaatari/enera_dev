﻿using System.Threading;
using System.Threading.Tasks;

namespace PV.Business.Abstractions.IEngines.Victron
{
    public interface IVictronIntegrationEngine
    {
        public Task DownloadInstallationCSV(CancellationToken ct);

        Task DownloadReadingKwh(CancellationToken ct);
    }
}
