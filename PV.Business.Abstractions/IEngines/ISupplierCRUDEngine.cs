﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PV.Business.Abstractions.IEngines
{
    public interface ISupplierCRUDEngine<Response, Request, UpdateRequest>
    {
        Task<Response> GetAsync(Guid supplierId, CancellationToken ct);
        Task<Response> GetAsNoTrackingAsync(Guid supplierId, CancellationToken ct);
        Task<Response> CreateAsync(Request supplierDTO, CancellationToken ct);
        Task<Response> UpdateAsync(UpdateRequest supplierDTO, CancellationToken ct);
        Task DeleteAsync(Guid supplierId, CancellationToken ct);
        Task<List<Response>> GetAllAsync(CancellationToken ct);
    }
}
