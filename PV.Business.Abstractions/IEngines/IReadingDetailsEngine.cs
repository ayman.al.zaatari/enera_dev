﻿using System;
using System.Threading;
using System.Threading.Tasks;
using PV.Business.DTO;

namespace PV.Business.Abstractions.IEngines
{
    public interface IReadingDetailsEngine
    {
        Task<ReadingDetailsDTO> GetAllByReadingIdAsync(Guid readingId, int count, CancellationToken ct);
    }
}
