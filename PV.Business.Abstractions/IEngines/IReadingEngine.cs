﻿using System;
using System.Threading;
using System.Threading.Tasks;
using PV.Business.DTO;

namespace PV.Business.Abstractions.IEngines
{
    public interface IReadingEngine
    {
        Task<ReadingsDTO> GetReadingsBySiteAsync(int idSite, int count, CancellationToken ct);
        Task<DiagnosticsDataDTO> GetDiagnosticsDataByReadingAsync(Guid idSite, CancellationToken ct);
    }
}
