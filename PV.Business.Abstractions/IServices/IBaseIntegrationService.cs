﻿using System.Threading;
using System.Threading.Tasks;
using PV.Business.DTO.IntegrationResponseDTO.Base;
using PV.Business.DTO.IntegrationResponseDTO.Victron;

namespace PV.Service.Business.Astractions.IServices
{
    public interface IBaseIntegrationService<Settings, LoginResponse, InstallationsResponse, InstallationResponse, ConnectedDevicesResponse, DiagnosticsDataResponse, ReadingsKwhResponse>
        where LoginResponse : LoginResponseIntegrationDTO
        where InstallationsResponse : InstallationsIntegrationResponseDTO<InstallationResponse>
        where InstallationResponse : InstallationResponseIntegrationDTO
        where ConnectedDevicesResponse : ConnectedDevicesIntegrationDTO
        where DiagnosticsDataResponse : DiagnosticsDataIntegrationDTO
       where ReadingsKwhResponse : VictronReadingKwhDTO
    {
        Task<LoginResponse> AuthenticateSupplierAsync(CancellationToken ct);
        Task<InstallationsResponse> GetAllInstallationsAsync(CancellationToken ct);
        Task<ConnectedDevicesResponse> GetConnectedDevicesAsync(int siteId, CancellationToken ct);
        Task<DiagnosticsDataResponse> GetDiagnosticsDataAsync(int siteId, int count, CancellationToken ct);
        Task<string> GetCsvReadingsAsync(int siteId, double start, double end, CancellationToken ct);
        Task<ReadingsKwhResponse> GetReadingsKwhAsync(int siteId, double start, double end, CancellationToken ct);
    }
}