﻿using System;
using Newtonsoft.Json;

namespace PV.Core.Models
{
    public class KwhReading : BaseModel
    {
        public decimal? PbValue { get; set; }
        public decimal? PcValue { get; set; }
        public decimal? GbValue { get; set; }
        public decimal? GcValue { get; set; }
        public decimal? PgValue { get; set; }
        public decimal? BcValue { get; set; }
        public decimal? KwhValue { get; set; }
        public decimal? BgValue { get; set; }
        public DateTimeOffset ReadingDate { get; set; }

        [JsonIgnore]
        public Site Site { get; set; }
    }
}
