﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PV.Core.Models
{
    public class BaseModel
    {
        [Key]
        public Guid Id { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
