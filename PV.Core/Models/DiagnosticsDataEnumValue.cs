﻿using System;
using Newtonsoft.Json;

namespace PV.Core.Models
{
    public class DiagnosticsDataEnumValue : BaseModel
    {
        public Guid DiagnosticsDataId { get; set; }
        public string NameEnum { get; set; }
        public int ValueEnum { get; set; }

        [JsonIgnore]
        public DiagnosticsData DiagnosticsData { get; set; }
    }
}