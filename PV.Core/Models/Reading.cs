﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace PV.Core.Models
{
    public class Reading : BaseModel
    {
        public Reading()
        {
            DiagnosticsData = new HashSet<DiagnosticsData>();
        }

        public Guid SiteId { get; set; }

        [JsonIgnore]
        public ICollection<DiagnosticsData> DiagnosticsData { get; set; }

        [JsonIgnore]
        public Site Site { get; set; }
    }
}