﻿using System;

namespace PV.Core.Models
{
    public class LoadSchedding : BaseModel
    {
        public DateTimeOffset Date { get; set; }
        public string Day { get; set; }
        public string ZeroToSix { get; set; }
        public string SixToTen { get; set; }
        public string TenToFourteen { get; set; }
        public string FourteenToEighteen { get; set; }
        public string EighteenToTwentyFour { get; set; }
    }
}
