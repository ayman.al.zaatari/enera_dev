﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PV.Core.Models
{
    public class DiagnosticsData : BaseModel
    {
        public DiagnosticsData()
        {
            DataAttributeEnumValues = new HashSet<DiagnosticsDataEnumValue>();
        }
        public int IdSite { get; set; }
        public DateTimeOffset? Timestamp { get; set; }
        public string Device { get; set; }
        public int Instance { get; set; }
        public int IdDataAttribute { get; set; }
        public string Description { get; set; }
        public string FormatWithUnit { get; set; }
        public string DbusServiceType { get; set; }
        public string DbusPath { get; set; }
        public string FormattedValue { get; set; }
        public int RequestId { get; set; }
        public Guid ReadingId { get; set; }

        [JsonIgnore]
        public ICollection<DiagnosticsDataEnumValue> DataAttributeEnumValues { get; set; }
        [JsonIgnore]
        public Reading Reading { get; set; }
    }
}