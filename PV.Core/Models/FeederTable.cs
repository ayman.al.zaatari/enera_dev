﻿namespace PV.Core.Models
{
    public class FeederTable : BaseModel
    {
        public string FeederID { get; set; }
        public string FeederName { get; set; }
        public string PrimarySubstation { get; set; }
        public string Status { get; set; }
        public string IP { get; set; }
        public string AddressLow { get; set; }
        public string M3Validity { get; set; }
    }
}