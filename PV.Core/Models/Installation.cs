﻿namespace PV.Core.Models
{
    public class Installation : BaseModel
    {
        public int IdSite { get; set; }
        public string Name { get; set; }
        public int IdUser { get; set; }
        public int PvMax { get; set; }
        public bool ReportsEnabled { get; set; }
        public int AccessLevel { get; set; }
        public string Timezone { get; set; }
        public bool Owner { get; set; }
        public bool Geofence { get; set; }
        public bool GeofenceEnabled { get; set; }
        public string DeviceIcon { get; set; }
    }
}
