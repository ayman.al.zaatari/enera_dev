﻿namespace PV.Core.Models
{
    public class Supplier : BaseModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
