﻿using System.Collections.Generic;
using Newtonsoft.Json;
using PV.Core.Models;

namespace PV.Core.Models
{
    public class Site : BaseModel
    {
        public Site()
        {
            Readings = new HashSet<Reading>();
            KwhReadings = new HashSet<KwhReading>();
            CsvReadings = new HashSet<CsvReading>();
        }

        public int IdSite { get; set; }

        [JsonIgnore]
        public ICollection<Reading> Readings { get; set; }
        [JsonIgnore]
        public ICollection<CsvReading> CsvReadings { get; set; }
        [JsonIgnore]
        public ICollection<KwhReading> KwhReadings { get; set; }
    }
}