using System;
using Xunit;
using Moq;
using PV.Service.Business.Engines.Victron;
using PV.DataAccess.Abstractions.IUnitOfWork;
using Microsoft.Extensions.Logging;
using AutoMapper;
using System.Threading;
using PV.Core.Models;
using PV.Business.DTO;

namespace PV.Tests
{
    public class VictronCRUDEngineTest
    {
        public VictronCRUDEngine _victronEngine;
        public Mock<IUnitOfWork> _mockUow { get; set; }
        public Mock<ILogger<VictronCRUDEngine>> _mockLogger { get; set; }
        public Mock<IMapper> _mockMapper { get; set; }
        public Supplier Supplier { get; set; }
        public VictronCRUDEngineTest()
        {
            _mockUow = new Mock<IUnitOfWork>();
            _mockLogger = new Mock<ILogger<VictronCRUDEngine>>();
            _mockMapper = new Mock<IMapper>();
            _victronEngine = new VictronCRUDEngine(_mockUow.Object, _mockMapper.Object, _mockLogger.Object);

            Supplier = new Supplier()
            {
                Key = "Key"
            };
        }

        [Fact]
        public void GetAsync_WhenNull()
        {
            //Arrange
            _mockUow.Setup(x => x.Suppliers.GetAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>(), It.IsAny<bool>())).ReturnsAsync(Supplier);

            _mockMapper.Setup(x => x.Map<SupplierResponseDTO>(It.IsAny<Supplier>())).Returns(new SupplierResponseDTO { Key = "Key" });

            //Act
            var result = _victronEngine.GetAsync(Guid.NewGuid(), new CancellationToken()).Result;

            //Assert
            Assert.IsType<SupplierResponseDTO>(result);
            Assert.Equal("Key", result.Key);
        }
    }
}
