﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PV.Service.Business.Engines;

namespace PV.Controllers
{

    public class SupplierController : BaseController
    {
        public SupplierLoginEngine _supplierLoginEngine { get; set; }

        public SupplierController(SupplierLoginEngine supplierLoginEngine,
            ILogger<SupplierController> logger) : base(logger)
        {
            _supplierLoginEngine = supplierLoginEngine;
        }

        [HttpPost]
        [Route("{SupplierId}/Login")]
        public async Task<IActionResult> LoginSupplier(Guid SupplierId, CancellationToken ct)
        {
            var result = await _supplierLoginEngine.AuthenticateWithSupplier(SupplierId);

            return Ok(result);
        }
    }
}
