﻿using System;

namespace PV.Service.Business.Helpers
{
    public static class DateHelper
    {
        public static DateTime? UnixTimeStampToDateTime(double? unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            if (unixTimeStamp.HasValue)
            {
                DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(unixTimeStamp.Value);//.ToLocalTime();
                return dtDateTime;
            }
            return null;
        }

        public static double? DateTimeToUnixTimeStamp(DateTimeOffset? date)
        {
            // Unix timestamp is seconds past epoch
            if (date.HasValue)
            {
                return (Int32)(date.Value.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            }
            return null;
        }
    }
}
