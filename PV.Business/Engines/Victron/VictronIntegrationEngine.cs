﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using PV.Business.Abstractions.IEngines.Victron;
using PV.Business.DTO.IntegrationResponseDTO;
using PV.Business.DTO.IntegrationResponseDTO.Victron;
using PV.Business.Settings.Victron;
using PV.Core.Models;
using PV.DataAccess.Abstractions.IUnitOfWork;
using PV.Service.Business.Astractions.IServices;
using PV.Service.Business.Engines.Base;
using PV.Service.Business.Helpers;
using PV.Service.Business.Utils;

namespace PV.Service.Business.Engines.Victron
{
    public class VictronIntegrationEngine : BaseIntegrationEngine<VictronSettings, VictronLoginResponseIntegrationDTO, VictronInstallationsResponseIntegrationDTO, VictronInstallationResponseIntegrationDTO, VictronConnectedDevicesIntegrationDTO, VictronDiagnosticsDataIntegrationDTO, DiagnosticsData, VictronReadingKwhDTO>, IVictronIntegrationEngine
    {
        public VictronIntegrationEngine(IUnitOfWork uow,
            IMapper map,
            ILogger<VictronIntegrationEngine> logger,
            IBaseIntegrationService<VictronSettings, VictronLoginResponseIntegrationDTO, VictronInstallationsResponseIntegrationDTO, VictronInstallationResponseIntegrationDTO, VictronConnectedDevicesIntegrationDTO, VictronDiagnosticsDataIntegrationDTO, VictronReadingKwhDTO> baseIntegration)
            : base(uow, map, logger, baseIntegration)
        {

        }

        public async Task DownloadInstallationCSV(CancellationToken ct)
        {
            //Get Last TImestamp
            var sites = await _uow.Sites.GetAll(ct);

            foreach (var site in sites)
            {
                DateTimeOffset? latestTimestamp = await _uow.CsvReadings.GetLatestTimeStamp(site.IdSite, ct);

                string code = ((Enums.Site)site.IdSite).ToString();

                AppSettings siteSettings = await _uow.AppSettings.GetByKey(code, ct);

                DateTimeOffset siteSettingsDate = DateTimeOffset.Parse(siteSettings.Value).UtcDateTime;

                if (!latestTimestamp.HasValue || (latestTimestamp.Value.Subtract(siteSettingsDate) >= TimeSpan.FromHours(6) && siteSettingsDate.CompareTo(DateTimeOffset.UtcNow) < 0))
                    latestTimestamp = siteSettingsDate;

                if (latestTimestamp.HasValue)
                {
                    //Api call
                    var csv = await _baseIntegration.GetCsvReadingsAsync(site.IdSite,
                        DateHelper.DateTimeToUnixTimeStamp(latestTimestamp.Value).Value,
                        DateHelper.DateTimeToUnixTimeStamp(latestTimestamp.Value.AddHours(6).AddMilliseconds(-1)).Value,
                        ct);

                    siteSettings.Value = latestTimestamp.Value.AddHours(6).ToString();

                    csv = csv.Replace("\r\n", ",");

                    string pattern = @"""\s*,\s*""";

                    // input.Substring(1, input.Length - 2) removes the first and last " from the string
                    string[] lines = System.Text.RegularExpressions.Regex.Split(csv.Substring(1, csv.Length - 2), pattern);

                    int max;

                    int columns;

                    if (site.IdSite == 42136)
                    {
                        columns = 79;
                        max = lines.Count() / columns;
                    }
                    else if (site.IdSite == 43166)
                    {
                        columns = 144;
                        max = lines.Count() / columns;
                    }
                    else
                    {
                        columns = 74;
                        max = lines.Count() / columns;
                    }


                    CsvReading csvReading;
                    try
                    {

                        for (int i = 3; i < max; i++)
                        {
                            csvReading = new CsvReading();
                            if (site.IdSite == 42136)
                            {
                                csvReading.TimeStamp = Convert.ToDateTime(lines[i * columns]);
                                csvReading.Gateway0_VrmLogTimeOffset = Convert.ToDecimal(lines[i * columns + 1]);
                                csvReading.Gateway0_EssScheduledCharging = lines[i * columns + 2];
                                csvReading.Gateway0_Relay1State = lines[i * columns + 3];
                                csvReading.Gateway0_GeneratorRunReason = lines[i * columns + 4];
                                csvReading.Gateway0_PhaseRotation = lines[i * columns + 5];
                                csvReading.Gateway0_InputVoltagePhase1 = Convert.ToDecimal(lines[i * columns + 6]);
                                csvReading.VeBusSystem0_InputCurrentPhase1 = Convert.ToDecimal(lines[i * columns + 7]);
                                csvReading.VeBusSystem0_InputFrequency1 = Convert.ToDecimal(lines[i * columns + 8]);
                                csvReading.VeBusSystem0_InputPower1 = Convert.ToDecimal(lines[i * columns + 9]);
                                csvReading.VeBusSystem0_OutputVoltagePhase1 = Convert.ToDecimal(lines[i * columns + 10]);
                                csvReading.VeBusSystem0_OutputCurrentPhase1 = Convert.ToDecimal(lines[i * columns + 11]);
                                csvReading.VeBusSystem0_OutputFrequency = Convert.ToDecimal(lines[i * columns + 12]);
                                csvReading.VeBusSystem0_OutputPower1 = Convert.ToDecimal(lines[i * columns + 13]);
                                csvReading.VeBusSystem0_Voltage = Convert.ToDecimal(lines[i * columns + 14]);
                                csvReading.VeBusSystem0_Current = Convert.ToDecimal(lines[i * columns + 15]);
                                csvReading.VeBusSystem0_ActiveInput = lines[i * columns + 16];
                                csvReading.VeBusSystem0_ActiveInputCurrentLimit = Convert.ToDecimal(lines[i * columns + 17]);
                                csvReading.VeBusSystem0_VeBusStateOfCharge = Convert.ToDecimal(lines[i * columns + 18]);
                                csvReading.VeBusSystem0_VeBusState = lines[i * columns + 19];
                                csvReading.VeBusSystem0_VeBusError = lines[i * columns + 20];
                                csvReading.VeBusSystem0_SwitchPosition = lines[i * columns + 21];
                                csvReading.VeBusSystem0_Temperature = lines[i * columns + 22];
                                csvReading.VeBusSystem0_LowBattery = lines[i * columns + 23];
                                csvReading.VeBusSystem0_Overload = lines[i * columns + 24];
                                csvReading.VeBusSystem0_ChargeState = lines[i * columns + 25];
                                csvReading.VeBusSystem0_TemperaturSensorAlarm = lines[i * columns + 26];
                                csvReading.VeBusSystem0_VoltageSensorAlarm = lines[i * columns + 27];
                                csvReading.VeBusSystem0_HighDcRipple = lines[i * columns + 28];
                                csvReading.VeBusSystem0_TemperatureL1 = lines[i * columns + 29];
                                csvReading.VeBusSystem0_LowBatteryL1 = lines[i * columns + 30];
                                csvReading.VeBusSystem0_OverloadL1 = lines[i * columns + 31];
                                csvReading.VeBusSystem0_HighDcRippleL1 = lines[i * columns + 32];
                                csvReading.VeBusSystem0_TemperatureL2 = lines[i * columns + 33];
                                csvReading.VeBusSystem0_LowBatteryL2 = lines[i * columns + 34];
                                csvReading.VeBusSystem0_OverloadL2 = lines[i * columns + 35];
                                csvReading.VeBusSystem0_HighDcRippleL2 = lines[i * columns + 36];
                                csvReading.VeBusSystem0_TemperatureL3 = lines[i * columns + 37];
                                csvReading.VeBusSystem0_LowBatteryL3 = lines[i * columns + 38];
                                csvReading.VeBusSystem0_OverloadL3 = lines[i * columns + 39];
                                csvReading.VeBusSystem0_HighDcRippleL3 = lines[i * columns + 40];
                                csvReading.SolarCharger_Voltage = Convert.ToDecimal(lines[i * columns + 41]);
                                csvReading.SolarCharger_Current = Convert.ToDecimal(lines[i * columns + 42]);
                                csvReading.SolarCharger_BatteryWatts = Convert.ToDecimal(lines[i * columns + 43]);
                                csvReading.SolarCharger_LoadState = lines[i * columns + 44];
                                csvReading.SolarCharger_ChargerOnOff = lines[i * columns + 45];
                                csvReading.SolarCharger_ChargeState = lines[i * columns + 46];
                                csvReading.SolarCharger_PvVoltage = Convert.ToDecimal(lines[i * columns + 47]);
                                csvReading.SolarCharger_PvCurrent = Convert.ToDecimal(lines[i * columns + 48]);
                                csvReading.SolarCharger_PvPower = Convert.ToDecimal(lines[i * columns + 49]);
                                csvReading.SolarCharger_MpptState = lines[i * columns + 50];
                                csvReading.SolarCharger_RelayOnTheCharger = lines[i * columns + 51];
                                csvReading.SolarCharger_YieldToday = Convert.ToDecimal(lines[i * columns + 52]);
                                csvReading.SolarCharger_MaximumChargePowerToday = Convert.ToDecimal(lines[i * columns + 53]);
                                csvReading.SolarCharger_YieldYesterday = Convert.ToDecimal(lines[i * columns + 54]);
                                csvReading.SolarCharger_MaximumChargePowerYesterday = Convert.ToDecimal(lines[i * columns + 55]);
                                csvReading.SolarCharger_ErrorCode = lines[i * columns + 56];
                                csvReading.SolarCharger_UserYield = Convert.ToDecimal(lines[i * columns + 57]);
                                csvReading.SystemOverview0_AcInput = lines[i * columns + 58];
                                csvReading.SystemOverview0_PvDcCoupled = lines[i * columns + 59];
                                csvReading.SystemOverview0_AcConsumptionL1 = Convert.ToDecimal(lines[i * columns + 60]);
                                csvReading.SystemOverview0_GridL1 = Convert.ToDecimal(lines[i * columns + 61]);
                                csvReading.SystemOverview0_Voltage = Convert.ToDecimal(lines[i * columns + 62]);
                                csvReading.SystemOverview0_BatteryStateOfCharge = Convert.ToDecimal(lines[i * columns + 63]);
                                csvReading.SystemOverview0_DvccMultipleBatteriesAlarm = lines[i * columns + 64];
                                csvReading.SystemOverview0_DvccFirmwareInsufficientAlarm = lines[i * columns + 65];
                                csvReading.SystemOverview0_OneLowSocDischargeDisabled = lines[i * columns + 66];
                                csvReading.SystemOverview0_TwoBatteryLifeIsActive = lines[i * columns + 67];
                                csvReading.SystemOverview0_ThreeChargeDisabledByBms = lines[i * columns + 68];
                                csvReading.SystemOverview0_FourDischargeDisabledByBms = lines[i * columns + 69];
                                csvReading.SystemOverview0_FiveSlowChargeIsActive = lines[i * columns + 70];
                                csvReading.SystemOverview0_SixChargeDisabledByUserSetting = lines[i * columns + 71];
                                csvReading.SystemOverview0_SevenDischargeDisabledByUserSetting = lines[i * columns + 72];
                                csvReading.SystemOverview0_Current = Convert.ToDecimal(lines[i * columns + 73]);
                                csvReading.SystemOverview0_VeBusChargeCurrent = Convert.ToDecimal(lines[i * columns + 74]);
                                csvReading.SystemOverview0_BatteryPower = lines[i * columns + 75];
                                csvReading.SystemOverview0_VeBusChargePower = lines[i * columns + 76];
                                csvReading.SystemOverview0_BatteryState = lines[i * columns + 77];
                                csvReading.SystemOverview0_GeneratorNotDetectedAtAcInput = lines[i * columns + 78];
                            }
                            else if (site.IdSite == 43166)
                            {
                                csvReading.TimeStamp = Convert.ToDateTime(lines[i * columns]);
                                csvReading.Gateway0_VrmLogTimeOffset = Convert.ToDecimal(lines[i * columns + 1]);
                                csvReading.Gateway0_EssScheduledCharging = lines[i * columns + 2];
                                csvReading.Gateway0_Relay1State = lines[i * columns + 3];
                                csvReading.Gateway0_GeneratorRunReason = lines[i * columns + 4];
                                csvReading.Gateway0_PhaseRotation = lines[i * columns + 5];
                                csvReading.Gateway0_InputVoltagePhase1 = Convert.ToDecimal(lines[i * columns + 6]);
                                csvReading.Gateway0_InputVoltagePhase2 = Convert.ToDecimal(lines[i * columns + 7]);
                                csvReading.Gateway0_InputVoltagePhase3 = Convert.ToDecimal(lines[i * columns + 8]);
                                csvReading.VeBusSystem0_InputCurrentPhase1 = Convert.ToDecimal(lines[i * columns + 9]);
                                csvReading.VeBusSystem0_InputCurrentPhase2 = Convert.ToDecimal(lines[i * columns + 10]);
                                csvReading.VeBusSystem0_InputCurrentPhase3 = Convert.ToDecimal(lines[i * columns + 11]);
                                csvReading.VeBusSystem0_InputFrequency1 = Convert.ToDecimal(lines[i * columns + 12]);
                                csvReading.VeBusSystem0_InputFrequency2 = Convert.ToDecimal(lines[i * columns + 13]);
                                csvReading.VeBusSystem0_InputFrequency3 = Convert.ToDecimal(lines[i * columns + 14]);
                                csvReading.VeBusSystem0_InputPower1 = Convert.ToDecimal(lines[i * columns + 15]);
                                csvReading.VeBusSystem0_InputPower2 = Convert.ToDecimal(lines[i * columns + 16]);
                                csvReading.VeBusSystem0_InputPower3 = Convert.ToDecimal(lines[i * columns + 17]);
                                csvReading.VeBusSystem0_OutputVoltagePhase1 = Convert.ToDecimal(lines[i * columns + 18]);
                                csvReading.VeBusSystem0_OutputVoltagePhase2 = Convert.ToDecimal(lines[i * columns + 19]);
                                csvReading.VeBusSystem0_OutputVoltagePhase3 = Convert.ToDecimal(lines[i * columns + 20]);
                                csvReading.VeBusSystem0_OutputCurrentPhase1 = Convert.ToDecimal(lines[i * columns + 21]);
                                csvReading.VeBusSystem0_OutputCurrentPhase2 = Convert.ToDecimal(lines[i * columns + 22]);
                                csvReading.VeBusSystem0_OutputCurrentPhase3 = Convert.ToDecimal(lines[i * columns + 23]);
                                csvReading.VeBusSystem0_OutputFrequency = Convert.ToDecimal(lines[i * columns + 24]);
                                csvReading.VeBusSystem0_OutputPower1 = Convert.ToDecimal(lines[i * columns + 25]);
                                csvReading.VeBusSystem0_OutputPower2 = Convert.ToDecimal(lines[i * columns + 26]);
                                csvReading.VeBusSystem0_OutputPower3 = Convert.ToDecimal(lines[i * columns + 27]);
                                csvReading.VeBusSystem0_Voltage = Convert.ToDecimal(lines[i * columns + 28]);
                                csvReading.VeBusSystem0_Current = Convert.ToDecimal(lines[i * columns + 29]);
                                csvReading.VeBusSystem0_ActiveInput = lines[i * columns + 30];
                                csvReading.VeBusSystem0_ActiveInputCurrentLimit = Convert.ToDecimal(lines[i * columns + 31]);
                                csvReading.VeBusSystem0_VeBusStateOfCharge = Convert.ToDecimal(lines[i * columns + 32]);
                                csvReading.VeBusSystem0_VeBusState = lines[i * columns + 33];
                                csvReading.VeBusSystem0_VeBusError = lines[i * columns + 34];
                                csvReading.VeBusSystem0_SwitchPosition = lines[i * columns + 35];
                                csvReading.VeBusSystem0_Temperature = lines[i * columns + 36];
                                csvReading.VeBusSystem0_LowBattery = lines[i * columns + 37];
                                csvReading.VeBusSystem0_Overload = lines[i * columns + 38];
                                csvReading.VeBusSystem0_TemperaturSensorAlarm = lines[i * columns + 39];
                                csvReading.VeBusSystem0_VoltageSensorAlarm = lines[i * columns + 40];
                                csvReading.VeBusSystem0_HighDcRipple = lines[i * columns + 41];
                                csvReading.VeBusSystem0_TemperatureL1 = lines[i * columns + 42];
                                csvReading.VeBusSystem0_LowBatteryL1 = lines[i * columns + 43];
                                csvReading.VeBusSystem0_OverloadL1 = lines[i * columns + 44];
                                csvReading.VeBusSystem0_HighDcRippleL1 = lines[i * columns + 45];
                                csvReading.VeBusSystem0_TemperatureL2 = lines[i * columns + 46];
                                csvReading.VeBusSystem0_LowBatteryL2 = lines[i * columns + 47];
                                csvReading.VeBusSystem0_OverloadL2 = lines[i * columns + 48];
                                csvReading.VeBusSystem0_HighDcRippleL2 = lines[i * columns + 49];
                                csvReading.VeBusSystem0_TemperatureL3 = lines[i * columns + 50];
                                csvReading.VeBusSystem0_LowBatteryL3 = lines[i * columns + 51];
                                csvReading.VeBusSystem0_OverloadL3 = lines[i * columns + 52];
                                csvReading.VeBusSystem0_HighDcRippleL3 = lines[i * columns + 53];
                                csvReading.VeBusSystem0_ChargeState = lines[i * columns + 54];
                                csvReading.SolarCharger_Voltage = Convert.ToDecimal(lines[i * columns + 55]);
                                csvReading.SolarCharger_Current = Convert.ToDecimal(lines[i * columns + 56]);
                                csvReading.SolarCharger_BatteryWatts = Convert.ToDecimal(lines[i * columns + 57]);
                                csvReading.SolarCharger_LoadState = lines[i * columns + 58];
                                csvReading.SolarCharger_ChargerOnOff = lines[i * columns + 59];
                                csvReading.SolarCharger_ChargeState = lines[i * columns + 60];
                                csvReading.SolarCharger_PvVoltage = Convert.ToDecimal(lines[i * columns + 61]);
                                csvReading.SolarCharger_PvCurrent = Convert.ToDecimal(lines[i * columns + 62]);
                                csvReading.SolarCharger_PvPower = Convert.ToDecimal(lines[i * columns + 63]);
                                csvReading.SolarCharger_RelayOnTheCharger = lines[i * columns + 64];
                                csvReading.SolarCharger_YieldToday = Convert.ToDecimal(lines[i * columns + 65]);
                                csvReading.SolarCharger_MaximumChargePowerToday = Convert.ToDecimal(lines[i * columns + 66]);
                                csvReading.SolarCharger_YieldYesterday = Convert.ToDecimal(lines[i * columns + 67]);
                                csvReading.SolarCharger_MaximumChargePowerYesterday = Convert.ToDecimal(lines[i * columns + 68]);
                                csvReading.SolarCharger_ErrorCode = lines[i * columns + 69];
                                csvReading.SolarCharger_UserYield = Convert.ToDecimal(lines[i * columns + 70]);
                                csvReading.SolarCharger_Voltage2 = Convert.ToDecimal(lines[i * columns + 71]);
                                csvReading.SolarCharger_Current2 = Convert.ToDecimal(lines[i * columns + 72]);
                                csvReading.SolarCharger_BatteryWatts2 = Convert.ToDecimal(lines[i * columns + 73]);
                                csvReading.SolarCharger_LoadState2 = lines[i * columns + 74];
                                csvReading.SolarCharger_ChargerOnOff2 = lines[i * columns + 75];
                                csvReading.SolarCharger_ChargeState2 = lines[i * columns + 76];
                                csvReading.SolarCharger_PvVoltage2 = Convert.ToDecimal(lines[i * columns + 77]);
                                csvReading.SolarCharger_PvCurrent2 = Convert.ToDecimal(lines[i * columns + 78]);
                                csvReading.SolarCharger_PvPower2 = Convert.ToDecimal(lines[i * columns + 79]);
                                csvReading.SolarCharger_RelayOnTheCharger2 = lines[i * columns + 80];
                                csvReading.SolarCharger_YieldToday2 = Convert.ToDecimal(lines[i * columns + 81]);
                                csvReading.SolarCharger_MaximumChargePowerToday2 = Convert.ToDecimal(lines[i * columns + 82]);
                                csvReading.SolarCharger_YieldYesterday2 = Convert.ToDecimal(lines[i * columns + 83]);
                                csvReading.SolarCharger_MaximumChargePowerYesterday2 = Convert.ToDecimal(lines[i * columns + 84]);
                                csvReading.SolarCharger_ErrorCode2 = lines[i * columns + 85];
                                csvReading.SolarCharger_UserYield2 = Convert.ToDecimal(lines[i * columns + 86]);
                                csvReading.SolarCharger_Voltage3 = Convert.ToDecimal(lines[i * columns + 87]);
                                csvReading.SolarCharger_Current3 = Convert.ToDecimal(lines[i * columns + 88]);
                                csvReading.SolarCharger_BatteryWatts3 = Convert.ToDecimal(lines[i * columns + 89]);
                                csvReading.SolarCharger_LoadState3 = lines[i * columns + 90];
                                csvReading.SolarCharger_ChargerOnOff3 = lines[i * columns + 91];
                                csvReading.SolarCharger_ChargeState3 = lines[i * columns + 92];
                                csvReading.SolarCharger_PvVoltage3 = Convert.ToDecimal(lines[i * columns + 93]);
                                csvReading.SolarCharger_PvCurrent3 = Convert.ToDecimal(lines[i * columns + 94]);
                                csvReading.SolarCharger_PvPower3 = Convert.ToDecimal(lines[i * columns + 95]);
                                csvReading.SolarCharger_RelayOnTheCharger3 = lines[i * columns + 96];
                                csvReading.SolarCharger_YieldToday3 = Convert.ToDecimal(lines[i * columns + 97]);
                                csvReading.SolarCharger_MaximumChargePowerToday3 = Convert.ToDecimal(lines[i * columns + 98]);
                                csvReading.SolarCharger_YieldYesterday3 = Convert.ToDecimal(lines[i * columns + 99]);
                                csvReading.SolarCharger_MaximumChargePowerYesterday3 = Convert.ToDecimal(lines[i * columns + 100]);
                                csvReading.SolarCharger_ErrorCode3 = lines[i * columns + 101];
                                csvReading.SolarCharger_UserYield3 = Convert.ToDecimal(lines[i * columns + 102]);
                                csvReading.SolarCharger_Voltage4 = Convert.ToDecimal(lines[i * columns + 103]);
                                csvReading.SolarCharger_Current4 = Convert.ToDecimal(lines[i * columns + 104]);
                                csvReading.SolarCharger_BatteryWatts4 = Convert.ToDecimal(lines[i * columns + 105]);
                                csvReading.SolarCharger_LoadState4 = lines[i * columns + 106];
                                csvReading.SolarCharger_ChargerOnOff4 = lines[i * columns + 107];
                                csvReading.SolarCharger_ChargeState4 = lines[i * columns + 108];
                                csvReading.SolarCharger_PvVoltage4 = Convert.ToDecimal(lines[i * columns + 109]);
                                csvReading.SolarCharger_PvCurrent4 = Convert.ToDecimal(lines[i * columns + 110]);
                                csvReading.SolarCharger_PvPower4 = Convert.ToDecimal(lines[i * columns + 111]);
                                csvReading.SolarCharger_RelayOnTheCharger4 = lines[i * columns + 112];
                                csvReading.SolarCharger_YieldToday4 = Convert.ToDecimal(lines[i * columns + 113]);
                                csvReading.SolarCharger_MaximumChargePowerToday4 = Convert.ToDecimal(lines[i * columns + 114]);
                                csvReading.SolarCharger_YieldYesterday4 = Convert.ToDecimal(lines[i * columns + 115]);
                                csvReading.SolarCharger_MaximumChargePowerYesterday4 = Convert.ToDecimal(lines[i * columns + 116]);
                                csvReading.SolarCharger_ErrorCode4 = lines[i * columns + 117];
                                csvReading.SolarCharger_UserYield4 = Convert.ToDecimal(lines[i * columns + 118]);
                                csvReading.SystemOverview0_AcInput = lines[i * columns + 119];
                                csvReading.SystemOverview0_PvDcCoupled = lines[i * columns + 120];
                                csvReading.SystemOverview0_AcConsumptionL1 = Convert.ToDecimal(lines[i * columns + 121]);
                                csvReading.SystemOverview0_AcConsumptionL2 = Convert.ToDecimal(lines[i * columns + 122]);
                                csvReading.SystemOverview0_AcConsumptionL3 = Convert.ToDecimal(lines[i * columns + 123]);
                                csvReading.SystemOverview0_GridL1 = Convert.ToDecimal(lines[i * columns + 124]);
                                csvReading.SystemOverview0_GridL2 = Convert.ToDecimal(lines[i * columns + 125]);
                                csvReading.SystemOverview0_GridL3 = Convert.ToDecimal(lines[i * columns + 126]);
                                csvReading.SystemOverview0_Voltage = Convert.ToDecimal(lines[i * columns + 127]);
                                csvReading.SystemOverview0_BatteryStateOfCharge = Convert.ToDecimal(lines[i * columns + 128]);
                                csvReading.SystemOverview0_DvccMultipleBatteriesAlarm = lines[i * columns + 129];
                                csvReading.SystemOverview0_DvccFirmwareInsufficientAlarm = lines[i * columns + 130];
                                csvReading.SystemOverview0_OneLowSocDischargeDisabled = lines[i * columns + 131];
                                csvReading.SystemOverview0_TwoBatteryLifeIsActive = lines[i * columns + 132];
                                csvReading.SystemOverview0_ThreeChargeDisabledByBms = lines[i * columns + 133];
                                csvReading.SystemOverview0_FourDischargeDisabledByBms = lines[i * columns + 134];
                                csvReading.SystemOverview0_FiveSlowChargeIsActive = lines[i * columns + 135];
                                csvReading.SystemOverview0_SixChargeDisabledByUserSetting = lines[i * columns + 136];
                                csvReading.SystemOverview0_SevenDischargeDisabledByUserSetting = lines[i * columns + 137];
                                csvReading.SystemOverview0_Current = Convert.ToDecimal(lines[i * columns + 138]);
                                csvReading.SystemOverview0_VeBusChargeCurrent = Convert.ToDecimal(lines[i * columns + 139]);
                                csvReading.SystemOverview0_BatteryPower = lines[i * columns + 140];
                                csvReading.SystemOverview0_VeBusChargePower = lines[i * columns + 141];
                                csvReading.SystemOverview0_BatteryState = lines[i * columns + 142];
                                csvReading.SystemOverview0_GeneratorNotDetectedAtAcInput = lines[i * columns + 143];

                            }
                            else
                            {
                                csvReading.TimeStamp = Convert.ToDateTime(lines[i * columns]);
                                csvReading.Gateway0_VrmLogTimeOffset = Convert.ToDecimal(lines[i * columns + 1]);
                                csvReading.Gateway0_EssScheduledCharging = lines[i * columns + 2];
                                csvReading.Gateway0_Relay1State = lines[i * columns + 3];
                                csvReading.Gateway0_PhaseRotation = lines[i * columns + 4];
                                csvReading.Gateway0_InputVoltagePhase1 = Convert.ToDecimal(lines[i * columns + 5]);
                                csvReading.VeBusSystem0_InputCurrentPhase1 = Convert.ToDecimal(lines[i * columns + 6]);
                                csvReading.VeBusSystem0_InputFrequency1 = Convert.ToDecimal(lines[i * columns + 7]);
                                csvReading.VeBusSystem0_InputPower1 = Convert.ToDecimal(lines[i * columns + 8]);
                                csvReading.VeBusSystem0_OutputVoltagePhase1 = Convert.ToDecimal(lines[i * columns + 9]);
                                csvReading.VeBusSystem0_OutputCurrentPhase1 = Convert.ToDecimal(lines[i * columns + 10]);
                                csvReading.VeBusSystem0_OutputFrequency = Convert.ToDecimal(lines[i * columns + 11]);
                                csvReading.VeBusSystem0_OutputPower1 = Convert.ToDecimal(lines[i * columns + 12]);
                                csvReading.VeBusSystem0_Voltage = Convert.ToDecimal(lines[i * columns + 13]);
                                csvReading.VeBusSystem0_Current = Convert.ToDecimal(lines[i * columns + 14]);
                                csvReading.VeBusSystem0_ActiveInput = lines[i * columns + 15];
                                csvReading.VeBusSystem0_ActiveInputCurrentLimit = Convert.ToDecimal(lines[i * columns + 16]);
                                csvReading.VeBusSystem0_VeBusStateOfCharge = Convert.ToDecimal(lines[i * columns + 17]);
                                csvReading.VeBusSystem0_VeBusState = lines[i * columns + 18];
                                csvReading.VeBusSystem0_VeBusError = lines[i * columns + 19];
                                csvReading.VeBusSystem0_SwitchPosition = lines[i * columns + 20];
                                csvReading.VeBusSystem0_Temperature = lines[i * columns + 21];
                                csvReading.VeBusSystem0_LowBattery = lines[i * columns + 22];
                                csvReading.VeBusSystem0_Overload = lines[i * columns + 23];
                                csvReading.VeBusSystem0_TemperaturSensorAlarm = lines[i * columns + 24];
                                csvReading.VeBusSystem0_VoltageSensorAlarm = lines[i * columns + 25];
                                csvReading.VeBusSystem0_HighDcRipple = lines[i * columns + 26];
                                csvReading.VeBusSystem0_TemperatureL1 = lines[i * columns + 27];
                                csvReading.VeBusSystem0_LowBatteryL1 = lines[i * columns + 28];
                                csvReading.VeBusSystem0_OverloadL1 = lines[i * columns + 29];
                                csvReading.VeBusSystem0_HighDcRippleL1 = lines[i * columns + 30];
                                csvReading.VeBusSystem0_TemperatureL2 = lines[i * columns + 31];
                                csvReading.VeBusSystem0_LowBatteryL2 = lines[i * columns + 32];
                                csvReading.VeBusSystem0_OverloadL2 = lines[i * columns + 33];
                                csvReading.VeBusSystem0_HighDcRippleL2 = lines[i * columns + 34];
                                csvReading.VeBusSystem0_TemperatureL3 = lines[i * columns + 35];
                                csvReading.VeBusSystem0_LowBatteryL3 = lines[i * columns + 36];
                                csvReading.VeBusSystem0_OverloadL3 = lines[i * columns + 37];
                                csvReading.VeBusSystem0_HighDcRippleL3 = lines[i * columns + 38];
                                csvReading.SolarCharger_Voltage = Convert.ToDecimal(lines[i * columns + 39]);
                                csvReading.SolarCharger_Current = Convert.ToDecimal(lines[i * columns + 40]);
                                csvReading.SolarCharger_BatteryWatts = Convert.ToDecimal(lines[i * columns + 41]);
                                csvReading.SolarCharger_LoadState = lines[i * columns + 42];
                                csvReading.SolarCharger_ChargerOnOff = lines[i * columns + 43];
                                csvReading.SolarCharger_ChargeState = lines[i * columns + 44];
                                csvReading.SolarCharger_PvVoltage = Convert.ToDecimal(lines[i * columns + 45]);
                                csvReading.SolarCharger_PvCurrent = Convert.ToDecimal(lines[i * columns + 46]);
                                csvReading.SolarCharger_PvPower = Convert.ToDecimal(lines[i * columns + 47]);
                                csvReading.SolarCharger_MpptState = lines[i * columns + 48];
                                csvReading.SolarCharger_RelayOnTheCharger = lines[i * columns + 49];
                                csvReading.SolarCharger_YieldToday = Convert.ToDecimal(lines[i * columns + 50]);
                                csvReading.SolarCharger_MaximumChargePowerToday = Convert.ToDecimal(lines[i * columns + 51]);
                                csvReading.SolarCharger_YieldYesterday = Convert.ToDecimal(lines[i * columns + 52]);
                                csvReading.SolarCharger_MaximumChargePowerYesterday = Convert.ToDecimal(lines[i * columns + 53]);
                                csvReading.SolarCharger_ErrorCode = lines[i * columns + 54];
                                csvReading.SolarCharger_UserYield = Convert.ToDecimal(lines[i * columns + 55]);
                                csvReading.SystemOverview0_AcInput = lines[i * columns + 56];
                                csvReading.SystemOverview0_PvDcCoupled = lines[i * columns + 57];
                                csvReading.SystemOverview0_AcConsumptionL1 = Convert.ToDecimal(lines[i * columns + 58]);
                                csvReading.SystemOverview0_GridL1 = Convert.ToDecimal(lines[i * columns + 59]);
                                csvReading.SystemOverview0_Voltage = Convert.ToDecimal(lines[i * columns + 60]);
                                csvReading.SystemOverview0_BatteryStateOfCharge = Convert.ToDecimal(lines[i * columns + 61]);
                                csvReading.SystemOverview0_OneLowSocDischargeDisabled = lines[i * columns + 62];
                                csvReading.SystemOverview0_TwoBatteryLifeIsActive = lines[i * columns + 63];
                                csvReading.SystemOverview0_ThreeChargeDisabledByBms = lines[i * columns + 64];
                                csvReading.SystemOverview0_FourDischargeDisabledByBms = lines[i * columns + 65];
                                csvReading.SystemOverview0_FiveSlowChargeIsActive = lines[i * columns + 66];
                                csvReading.SystemOverview0_SixChargeDisabledByUserSetting = lines[i * columns + 67];
                                csvReading.SystemOverview0_SevenDischargeDisabledByUserSetting = lines[i * columns + 68];
                                csvReading.SystemOverview0_Current = Convert.ToDecimal(lines[i * columns + 69]);
                                csvReading.SystemOverview0_VeBusChargeCurrent = Convert.ToDecimal(lines[i * columns + 70]);
                                csvReading.SystemOverview0_BatteryPower = lines[i * columns + 71];
                                csvReading.SystemOverview0_VeBusChargePower = lines[i * columns + 72];
                                csvReading.SystemOverview0_BatteryState = lines[i * columns + 73];
                            }
                            csvReading.Site = site;

                            await _uow.CsvReadings.AddAsync(csvReading, ct);
                        }
                    }
                    catch (Exception)
                    {

                    }

                    await _uow.SaveChangesAsync(ct);
                }
            }
        }

        public async Task DownloadReadingKwh(CancellationToken ct)
        {
            //Get All sites
            var sites = await _uow.Sites.GetAll(ct);

            foreach (var site in sites)
            {
                DateTimeOffset? latestTimestamp = await _uow.CsvReadings.GetLatestTimeStamp(site.IdSite, ct);

                string code = ((Enums.SiteKwh)site.IdSite).ToString();

                AppSettings siteSettings = await _uow.AppSettings.GetByKey(code, ct);

                DateTimeOffset siteSettingsDate = DateTimeOffset.Parse(siteSettings.Value).UtcDateTime;

                if (!latestTimestamp.HasValue
                    || (latestTimestamp.Value.Subtract(siteSettingsDate) >= TimeSpan.FromDays(31) && siteSettingsDate.CompareTo(DateTimeOffset.UtcNow) < 0))
                    latestTimestamp = siteSettingsDate;

                if (latestTimestamp.HasValue)
                {
                    var result = await _baseIntegration.GetReadingsKwhAsync(site.IdSite,
                        DateHelper.DateTimeToUnixTimeStamp(latestTimestamp.Value).Value,
                        DateHelper.DateTimeToUnixTimeStamp(latestTimestamp.Value.AddDays(31)).Value,
                        ct);

                    if (result != null && result.Success)
                    {
                        List<KwhReading> kwhReadings = new List<KwhReading>();
                        try
                        {

                            for (int i = 0; i < result.Records.Bc.Count(); i++)
                            {
                                FillList(kwhReadings, result.Records.Bc[i], 1, site);
                            }
                            for (int i = 0; i < result.Records.Gb.Count(); i++)
                            {
                                FillList(kwhReadings, result.Records.Gb[i], 2, site);
                            }
                            for (int i = 0; i < result.Records.Gc.Count(); i++)
                            {
                                FillList(kwhReadings, result.Records.Gc[i], 3, site);
                            }
                            for (int i = 0; i < result.Records.Kwh.Count(); i++)
                            {
                                FillList(kwhReadings, result.Records.Kwh[i], 4, site);
                            }
                            for (int i = 0; i < result.Records.Pb.Count(); i++)
                            {
                                FillList(kwhReadings, result.Records.Pb[i], 5, site);
                            }
                            for (int i = 0; i < result.Records.Pc.Count(); i++)
                            {
                                FillList(kwhReadings, result.Records.Pc[i], 6, site);
                            }
                            for (int i = 0; i < result.Records.Pg.Count(); i++)
                            {
                                FillList(kwhReadings, result.Records.Pg[i], 7, site);
                            }
                            for (int i = 0; i < result.Records.Bg.Count(); i++)
                            {
                                FillList(kwhReadings, result.Records.Bg[i], 8, site);
                            }
                        }
                        catch(Exception e)
                        {
                            Console.Write("test");
                        }
                        await _uow.KwhReadings.AddRangeAsync(kwhReadings, ct);
                    }
                }

                siteSettings.Value = latestTimestamp.Value.AddDays(31).AddMilliseconds(1).ToString();
            }

            //Loopsites to get data
            await _uow.SaveChangesAsync(ct);
        }

        private void FillList(List<KwhReading> list, double[] item, int type, Site site)
        {
            var date = DateHelper.UnixTimeStampToDateTime(item[0] / 1000);

            var existing = list.FirstOrDefault(x => x.ReadingDate.Equals(date));

            if (existing == null)
            {
                existing = new KwhReading()
                {
                    ReadingDate = date.Value,
                };

                existing.Site = site;

                list.Add(existing);
            }
            switch (type)
            {
                case 1:
                    existing.BcValue = Convert.ToDecimal(item[1]);
                    break;
                case 2:
                    existing.GbValue = Convert.ToDecimal(item[1]);
                    break;
                case 3:
                    existing.GcValue = Convert.ToDecimal(item[1]);
                    break;
                case 4:
                    existing.KwhValue = Convert.ToDecimal(item[1]);
                    break;
                case 5:
                    existing.PbValue = Convert.ToDecimal(item[1]);
                    break;
                case 6:
                    existing.PcValue = Convert.ToDecimal(item[1]);
                    break;
                case 7:
                    existing.PgValue = Convert.ToDecimal(item[1]);
                    break;
                case 8:
                    existing.BgValue = Convert.ToDecimal(item[1]);
                    break;
            }
        }
    }
}