﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using PV.Business.Abstractions.IEngines.Victron;
using PV.Business.DTO;
using PV.DataAccess.Abstractions.IUnitOfWork;

namespace PV.Service.Business.Engines.Victron
{
    public class VictronCRUDEngine : BaseCRUDEngine<SupplierResponseDTO, SupplierRequestDTO, SupplierUpdateRequestDTO> , IVictronEngine
    {
        public VictronCRUDEngine(IUnitOfWork uow, IMapper map, ILogger<VictronCRUDEngine> logger) : base(uow, map, logger)
        {
            _uow = uow;
            _map = map;
            _logger = logger;
        }
    }
}
