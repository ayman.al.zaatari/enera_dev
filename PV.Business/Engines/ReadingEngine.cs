﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using PV.Business.Abstractions.IEngines;
using PV.Business.DTO;
using PV.Common.Exceptions;
using PV.DataAccess.Abstractions.IUnitOfWork;
using PV.Service.Business.Utils;

namespace PV.Service.Business.Engines.Victron
{
    public class ReadingEngine : IReadingEngine
    {
        public IUnitOfWork _uow { get; set; }
        public IMapper _map { get; set; }
        public ILogger<ReadingEngine> _logger { get; set; }
        public ReadingEngine(IUnitOfWork uow, IMapper map, ILogger<ReadingEngine> logger)
        {
            _uow = uow;
            _map = map;
            _logger = logger;
        }

        public async Task<ReadingsDTO> GetReadingsBySiteAsync(int idSite, int count, CancellationToken ct)
        {
            var reading = await _uow.Sites.GetSiteReadingsByExternalId(idSite, ct);

            if (reading == null)
                throw new BaseException(ExceptionConstants.SITE_NOT_FOUND_CODE, ExceptionConstants.SITE_NOT_FOUND_MESSAGE);

            var readings = _map.Map<List<ReadingDTO>>(reading.Readings);

            ReadingsDTO readingsDTO = new ReadingsDTO
            {
                Readings = readings
            };

            return readingsDTO;
        }

        public async Task<DiagnosticsDataDTO> GetDiagnosticsDataByReadingAsync(Guid readingId, CancellationToken ct)
        {
            var diagnosticsData = await _uow.DiagnosticsDatas.GetDiagnosticsDataByReading(readingId, ct);

            if (diagnosticsData != null && diagnosticsData.Any())
            {
                DiagnosticsDataDTO diagnosticsDataDTO = new DiagnosticsDataDTO
                {
                    List = _map.Map<List<DiagnosticDataDTO>>(diagnosticsData)
                };

                return diagnosticsDataDTO;
            }
            else
            {
                throw new BaseException(ExceptionConstants.DIAGNOSTICS_DATA_NOT_FOUND_CODE, ExceptionConstants.DIAGNOSTICS_DATA_NOT_FOUND_MESSAGE);
            }
        }
    }
}

