﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using PV.Business.Abstractions.IEngines.Base;
using PV.Business.DTO.IntegrationResponseDTO.Base;
using PV.Business.DTO.IntegrationResponseDTO.Victron;
using PV.Business.DTO.Settings.Base;
using PV.Common.Exceptions;
using PV.Core.Models;
using PV.DataAccess.Abstractions.IUnitOfWork;
using PV.Service.Business.Astractions.IServices;
using PV.Service.Business.Utils;

namespace PV.Service.Business.Engines.Base
{
    public class BaseIntegrationEngine<Settings, LoginResponse, InstallationsResponse, InstallationResponse, ConnectedDevicesResponse, DiagnosticsDataResponse, DiagnosticsDataEntity, ReadingKwh>
        : IBaseIntegrationEngine<Settings, LoginResponse, InstallationsResponse, InstallationResponse, ConnectedDevicesResponse, DiagnosticsDataResponse, DiagnosticsDataEntity, ReadingKwh>
        where LoginResponse : LoginResponseIntegrationDTO
        where InstallationsResponse : InstallationsIntegrationResponseDTO<InstallationResponse>
        where Settings : BaseSettings
        where InstallationResponse : InstallationResponseIntegrationDTO
        where ConnectedDevicesResponse : ConnectedDevicesIntegrationDTO
        where DiagnosticsDataResponse : DiagnosticsDataIntegrationDTO
        where DiagnosticsDataEntity : DiagnosticsData
        where ReadingKwh : VictronReadingKwhDTO
    {
        public IUnitOfWork _uow { get; set; }
        public IMapper _map { get; set; }
        public ILogger<BaseIntegrationEngine<Settings, LoginResponse, InstallationsResponse, InstallationResponse, ConnectedDevicesResponse, DiagnosticsDataResponse, DiagnosticsDataEntity, ReadingKwh>> _logger { get; set; }
        public IBaseIntegrationService<Settings, LoginResponse, InstallationsResponse, InstallationResponse, ConnectedDevicesResponse, DiagnosticsDataResponse, ReadingKwh> _baseIntegration { get; set; }
        public BaseIntegrationEngine(IUnitOfWork uow,
            IMapper map, ILogger<BaseIntegrationEngine<Settings, LoginResponse, InstallationsResponse, InstallationResponse, ConnectedDevicesResponse, DiagnosticsDataResponse, DiagnosticsDataEntity, ReadingKwh>> logger,
            IBaseIntegrationService<Settings, LoginResponse, InstallationsResponse, InstallationResponse, ConnectedDevicesResponse, DiagnosticsDataResponse, ReadingKwh> baseIntegration)
        {
            _uow = uow;
            _map = map;
            _logger = logger;
            _baseIntegration = baseIntegration;
        }

        public virtual async Task<LoginResponse> AuthenticateSupplierAsync(Guid supplierId, CancellationToken ct)
        {
            _logger.LogInformation("Entered => SupplierIntegrationEngine.AuthenticateSupplierAsync");

            var result = await _baseIntegration.AuthenticateSupplierAsync(ct);

            return result;
        }

        public virtual async Task<InstallationsResponse> GetAllInstallationsAsync(CancellationToken ct)
        {
            _logger.LogInformation("Entered => SupplierIntegrationEngine.GetAllInstallationsAsync");

            var result = await _baseIntegration.GetAllInstallationsAsync(ct);

            var allSites = await _uow.Sites.GetAll(ct);

            List<int> dbSites = new List<int>();

            if (allSites != null && allSites.Any())
                dbSites = allSites.Select(x => x.IdSite).ToList();

            List<int> apiSites = result.Records.Select(x => x.IdSite).ToList();

            var newSites = apiSites.Except(dbSites);

            foreach (var site in newSites)
            {
                await _uow.Sites.AddAsync(new Site
                {
                    IdSite = site
                }, ct);
            }

            await _uow.SaveChangesAsync(ct);

            return result;
        }

        public virtual async Task<InstallationResponse> GetInstallationAsync(int idSite, CancellationToken ct)
        {
            _logger.LogInformation("Entered => SupplierIntegrationEngine.GetAllInstallationsAsync");

            var result = await _baseIntegration.GetAllInstallationsAsync(ct);

            var record = result.Records.FirstOrDefault(x => x.IdSite.Equals(idSite));

            return record;
        }

        public virtual async Task<ConnectedDevicesResponse> GetSiteConnectedDevicesAsync(int siteId, CancellationToken ct)
        {
            _logger.LogInformation("Entered => SupplierIntegrationEngine.GetAllInstallationsAsync");

            var result = await _baseIntegration.GetConnectedDevicesAsync(siteId, ct);

            return result;
        }

        public virtual async Task<DiagnosticsDataResponse> GetDiagnosticsDataAsync(int siteId, int count, CancellationToken ct)
        {
            _logger.LogInformation("Entered => SupplierIntegrationEngine.GetAllInstallationsAsync");

            var result = await _baseIntegration.GetDiagnosticsDataAsync(siteId, count, ct);

            if (result.Success)
            {
                //await SaveReadingData(siteId, result, ct);
                return result;
            }
            else
            {
                throw new BaseException(ExceptionConstants.SUPPLIER_DIAGNOSTICS_DATA_CODE, ExceptionConstants.SUPPLIER_CREDENTIALS_NOT_FOUND_MESSAGE);
            }
        }

        public async Task SaveReadingData(int siteId, DiagnosticsDataResponse result, CancellationToken ct)
        {
            List<DiagnosticsDataEnumValue> enumValues = new List<DiagnosticsDataEnumValue>();

            List<DiagnosticsData> diagnosticsDatas = new List<DiagnosticsData>();

            DiagnosticsData diagnosticsData = new DiagnosticsData();

            List<DiagnosticsDataEnumValue> diagnosticsDataEnumValue = new List<DiagnosticsDataEnumValue>();

            foreach (var value in result.Records)
            {
                if (value != null)
                {
                    diagnosticsData = _map.Map<DiagnosticsData>(value);

                    diagnosticsDatas.Add(diagnosticsData);
                }

                if (value.DataAttributeEnumValues != null && value.DataAttributeEnumValues.Any())
                {
                    diagnosticsDataEnumValue = _map.Map<List<DiagnosticsDataEnumValue>>(value.DataAttributeEnumValues);

                    diagnosticsDataEnumValue.ForEach(x => x.DiagnosticsData = diagnosticsData);

                    enumValues.AddRange(diagnosticsDataEnumValue);
                }
            }

            Reading reading = new Reading();

            var givenSite = await _uow.Sites.GetSiteReadingsByExternalId(siteId, ct);

            if (givenSite != null)
                reading.Site = givenSite;

            await _uow.Readings.AddAsync(reading, ct);

            diagnosticsDatas.ForEach(x => x.Reading = reading);

            await _uow.DiagnosticsDatas.AddRangeAsync(diagnosticsDatas, ct);

            await _uow.DiagnosticsDataEnumValues.AddRangeAsync(enumValues, ct);

            await _uow.SaveChangesAsync(ct);
        }
    }
}