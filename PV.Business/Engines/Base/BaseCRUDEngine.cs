﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using PV.Business.Abstractions.IEngines;
using PV.Core.Models;
using PV.DataAccess.Abstractions.IUnitOfWork;

namespace PV.Service.Business.Engines.Victron
{
    public class BaseCRUDEngine<SupplierResponseDTO, SupplierRequestDTO, SupplierUpdateRequestDTO> : ISupplierCRUDEngine<SupplierResponseDTO, SupplierRequestDTO, SupplierUpdateRequestDTO>
    {
        public ILogger<BaseCRUDEngine<SupplierResponseDTO, SupplierRequestDTO, SupplierUpdateRequestDTO>> _logger { get; set; }
        public IMapper _map { get; set; }
        public IUnitOfWork _uow { get; set; }

        public BaseCRUDEngine(IUnitOfWork uow,
            IMapper map,
            ILogger<BaseCRUDEngine<SupplierResponseDTO, SupplierRequestDTO, SupplierUpdateRequestDTO>> logger)
        {
            _uow = uow;
            _map = map;
            _logger = logger;
        }

        public virtual async Task<SupplierResponseDTO> CreateAsync(SupplierRequestDTO supplierDTO, CancellationToken ct)
        {
            Supplier supplier = _map.Map<SupplierRequestDTO, Supplier>(supplierDTO);

            await _uow.Suppliers.AddAsync(supplier, ct);

            SupplierResponseDTO response = _map.Map<Supplier, SupplierResponseDTO>(supplier);

            await _uow.SaveChangesAsync(ct);

            return response;
        }

        public virtual async Task DeleteAsync(Guid supplierId, CancellationToken ct)
        {
            await _uow.Suppliers.DeleteAsync(supplierId, ct);

            await _uow.SaveChangesAsync(ct);
        }

        public virtual async Task<SupplierResponseDTO> GetAsNoTrackingAsync(Guid supplierId, CancellationToken ct)
        {
            var supplier = await _uow.Suppliers.GetAsNoTrackingAsync(supplierId, ct);

            var supplierDto = _map.Map<SupplierResponseDTO>(supplier);

            return supplierDto;
        }

        public virtual async Task<SupplierResponseDTO> GetAsync(Guid supplierId, CancellationToken ct)
        {
            var supplier = await _uow.Suppliers.GetAsync(supplierId, ct);

            var supplierDto = _map.Map<SupplierResponseDTO>(supplier);

            return supplierDto;
        }

        public virtual async Task<SupplierResponseDTO> UpdateAsync(SupplierUpdateRequestDTO supplierDTO, CancellationToken ct)
        {
            var updatedEntity = _map.Map<Supplier>(supplierDTO);

            await _uow.Suppliers.UpdateAsync(updatedEntity, ct);

            await _uow.SaveChangesAsync(ct);

            var result = _map.Map<Supplier, SupplierResponseDTO>(updatedEntity);

            return result;
        }

        public virtual async Task<List<SupplierResponseDTO>> GetAllAsync(CancellationToken ct)
        {
            var entities = await _uow.Suppliers.GetAll(ct);

            var result = _map.Map<List<Supplier>, List<SupplierResponseDTO>>(entities);

            return result;
        }
    }
}
