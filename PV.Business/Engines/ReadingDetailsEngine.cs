﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using PV.Business.Abstractions.IEngines;
using PV.Business.DTO;
using PV.Core.Models;
using PV.DataAccess.Abstractions.IUnitOfWork;

namespace PV.Service.Business.Engines.Victron
{
    public class ReadingDetailsEngine : IReadingDetailsEngine
    {
        public IUnitOfWork _uow { get; set; }
        public IMapper _map { get; set; }
        public ILogger<ReadingDetailsEngine> _logger { get; set; }
        public ReadingDetailsEngine(IUnitOfWork uow, IMapper map, ILogger<ReadingDetailsEngine> logger)
        {
            _uow = uow;
            _map = map;
            _logger = logger;
        }


        public async Task<ReadingDetailsDTO> GetAllByReadingIdAsync(Guid readingId, int count, CancellationToken ct)
        {
            var readingWithDiagnostics = await _uow.Readings.GetReadingWithDiagnosticsData(readingId, ct);
            //var reading = await _uow.Readings.GetAsync(readingId, ct);
            //var details = await _uow.DiagnosticsDataHistory.GetAll(readingId, count, ct);

            ReadingDetailsDTO readingsDetailsDTO = new ReadingDetailsDTO
            {
                Reading = readingWithDiagnostics,
                DiagnosticsDataDetails = _map.Map<List<DiagnosticsData>>(readingWithDiagnostics.DiagnosticsData)
            };

            return readingsDetailsDTO;
        }
    }
}

