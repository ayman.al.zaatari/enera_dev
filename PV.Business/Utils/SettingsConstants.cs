﻿namespace PV.Service.Business.Utils
{
    public class SettingsConstants
    {
        public const string VICTRON_AUTHENTICATION_USERNAME_KEY = "VICTRON_AUTHENTICATION_USERNAME";
        public const string VICTRON_AUTHENTICATION_PASSWORD_KEY = "VICTRON_AUTHENTICATION_PASSWORD";
    }
}
