﻿namespace PV.Service.Business.Utils
{
    public class ExceptionConstants
    {
        //Code
        public const string SUPPLIER_CREDENTIALS_NOT_FOUND_CODE = "supplier_credentials_not_found";
        public const string SUPPLIER_AUTHENTICATION_FAILED_CODE = "supplier_authentication_failed";
        public const string SUPPLIER_DIAGNOSTICS_DATA_CODE = "supplier_diagnostics_failed";
        public const string SITE_NOT_FOUND_CODE = "site_not_found";
        public const string DIAGNOSTICS_DATA_NOT_FOUND_CODE = "diagnostics_data_not_found";

        //Message
        public const string SUPPLIER_CREDENTIALS_NOT_FOUND_MESSAGE = "The credentials for this supplier could not be found.";
        public const string SUPPLIER_AUTHENTICATION_FAILED_MESSAGE = "Unable to login to supplier portal. Please try again later.";
        public const string SUPPLIER_DIAGNOSTICS_DATA_MESSAGE = "Could not get diagnostics data at this time. Please try again later.";
        public const string SITE_NOT_FOUND_MESSAGE = "The site in question could not be found.";
        public const string DIAGNOSTICS_DATA_NOT_FOUND_MESSAGE = "The diagnostics data in question could not be found.";
    }
}
