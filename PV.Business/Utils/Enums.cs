﻿namespace PV.Service.Business.Utils
{
    public class Enums
    {
        public enum Site
        {
            SelaataNextCsv = 43166,
            FermesNextCsv = 42136,
            RabihMNextCsv = 42249,
        }
        public enum SiteKwh
        {
            SelaataNextKwh = 43166,
            FermesNextKwh = 42136,
            RabihMNextKwh = 42249,
        }
    }
}
