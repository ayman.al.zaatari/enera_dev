﻿namespace PV.Service.Business.Utils
{
    public class BusinessConstants
    {
        public const string VictronAuthenticationEndpoint = "/auth/login";
        public const string VictronAllInstallationsEndpoint = "/users/{0}/installations";
        public const string VictronConnectedDevicesEndpoint = "/installations/{0}/system-overview";
        public const string VictronDiagnosticsDataEndpoint = "/installations/{0}/diagnostics?count={1}";
        public const string VictronCsvReadingsEndpoint = "/installations/{0}/data-download?start={1}&end={2}&format=csv";
        public const string VictronReadingsKwhEndpoint = "/installations/{0}/stats?type=kwh&start={1}&end={2}&interval=hours";
        public const string BearerString = "Bearer {0}";
        public const string MRBCFilePath = "location";
        //TODO: Fill location of file

        public const string DiagnosticsDataJobUpdateCronExpressionKey = "DiagnosticsDataJobUpdateCronExpressionKey";
    }
}
