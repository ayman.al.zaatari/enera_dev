﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PV.Business.Abstractions.IServices;
using PV.Business.DTO.IntegrationResponseDTO;
using PV.Business.DTO.IntegrationResponseDTO.Victron;
using PV.Business.Settings.Victron;

namespace PV.Service.Business.Services
{
    public class VictronIntegrationService : BaseIntegrationService<VictronSettings, VictronLoginResponseIntegrationDTO, VictronInstallationsResponseIntegrationDTO, VictronInstallationResponseIntegrationDTO, VictronConnectedDevicesIntegrationDTO, VictronDiagnosticsDataIntegrationDTO, VictronReadingKwhDTO>, IVictronIntegrationService
    {
        public VictronIntegrationService(IOptions<VictronSettings> settings, ILogger<VictronIntegrationService> logger) : base(settings, logger)
        {
        }
    }
}