﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PV.Business.Abstractions.IEngines.Victron;
using PV.DataAccess.Abstractions.IUnitOfWork;

namespace PV.Service.Business.Jobs
{
    public class GetCsvReadingsJob
    {
        public IUnitOfWork _uow { get; set; }
        public ILogger<GetCsvReadingsJob> _logger { get; set; }
        public IVictronIntegrationEngine _victronEngine { get; set; }
        public GetCsvReadingsJob(IVictronIntegrationEngine victronEngine,
            ILogger<GetCsvReadingsJob> logger,
            IUnitOfWork uow)
        {
            _logger = logger;
            _victronEngine = victronEngine;
            _uow = uow;
        }

        public async Task ExecuteAsync(CancellationToken ct)
        {
            await _victronEngine.DownloadInstallationCSV(ct);
        }
    }
}
