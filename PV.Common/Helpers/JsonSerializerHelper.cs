﻿using Newtonsoft.Json;

namespace PV.Common.Helpers
{
    public static class JsonSerializerHelper
    {
        public static string Serialize<T>(T t)
        {
            return JsonConvert.SerializeObject(t);
        }
        public static T Deserialize<T>(string jsonString)
        {
            return JsonConvert.DeserializeObject<T>(jsonString);
        }
    }
}
