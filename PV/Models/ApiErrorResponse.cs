﻿using PV.Common.Helpers;

namespace PV.WEB.Models
{
    public class ApiErrorResponse
    {
        public string ErrorCode { get; set; }
        public string Message { get; set; }

        public override string ToString()
        {
            return JsonSerializerHelper.Serialize(this);
        }
    }
}
