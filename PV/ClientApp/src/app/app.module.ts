import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { InstallationDetailsComponent } from './installation-details/installation-details.component';
import { InstallationsListComponent } from './installations-list/installations-list.component';
import { ConnectedDevicesComponent } from './connected-devices/connected-devices.component';
import { DiagnosticsDataDevicesComponent } from './diagnostics-data-devices/diagnostics-data-devices.component';
import { DeviceService } from './services/device.service';
import { ReadingComponent } from './reading-history/reading-history.component';
import { DiagnosticsDataDevicesReadingDetailsComponent } from './diagnostics-data-devices-reading-details/diagnostics-data-devices-reading-details.component';
import { DiagnosticsDataDevicesReadingsComponent } from './diagnostics-data-devices-readings/diagnostics-data-devices-readings.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    InstallationsListComponent,
    InstallationDetailsComponent,
    ConnectedDevicesComponent,
    DiagnosticsDataDevicesComponent,
    ReadingComponent,
    DiagnosticsDataDevicesReadingDetailsComponent,
    DiagnosticsDataDevicesReadingsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: InstallationsListComponent },
      { path: 'installation/:idSite', component: InstallationDetailsComponent },
      { path: 'installation/:idSite/devices', component: ConnectedDevicesComponent },
      { path: 'installation/:idSite/diagnostics', component: DiagnosticsDataDevicesComponent },
      { path: 'reading/:idSite/readings', component: ReadingComponent },
      { path: 'reading/:id/diagnosticsdata', component: DiagnosticsDataDevicesReadingDetailsComponent }
    ])
  ],
  providers: [DeviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
