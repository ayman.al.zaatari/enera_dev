import { Component, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHandler } from '@angular/common/http';

@Component({
    selector: 'app-diagnostics-data-devices',
    templateUrl: './diagnostics-data-devices.component.html'
})

export class DiagnosticsDataDevicesComponent {
    public diagnosticsDataDevices: DiagnosticsDataDevices;

    constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private router: ActivatedRoute) {
        this.router.params.subscribe(x => {
            http.get<DiagnosticsDataDevices>(baseUrl + 'api/supplierintegration/' + x.idSite + '/diagnostics').subscribe(result => {
                console.log(result);
                this.diagnosticsDataDevices = result;
            }, error => console.error(error));
        });
    }
}

export interface DiagnosticsDataDevices {
    diagnosticsDataDevices: DiagnosticsDataDevice[];
}


export interface DiagnosticsDataDevice {
    idSite: number;
    timestamp: number;
    device: string;
    instance: number;
    idDataAttribute: number;
    description: string;
    formatWithUnit: string;
    dbusServiceType: object;
    dbusPath: object;
    formattedValue: string;
    dataAttributeEnumValues: DataAttributeEnumValue[];
    id: number;
}

export interface DataAttributeEnumValue
{
    name: string;
    value: number;
}
