import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class DeviceService {
    //Observable source
    private idSource = new Subject<number>();
    //Observable stream
    public idEmitted = this.idSource.asObservable();
    constructor() {}

    setId(id:number){
        this.idSource.next(id);
    }

}