import { Component, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHandler } from '@angular/common/http';

@Component({
  selector: 'app-diagnostics-data-devices-readings',
  templateUrl: './diagnostics-data-devices-readings.component.html'
})

export class DiagnosticsDataDevicesReadingsComponent {
  public diagnosticsDataDevicesReadings: DiagnosticsDataDevicesReadings;

    constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private router: ActivatedRoute) {
        this.router.params.subscribe(x => {
          http.get<DiagnosticsDataDevicesReadings>(baseUrl + 'api/reading/' + x.idSite + '/readings').subscribe(result => {
                console.log(result);
              this.diagnosticsDataDevicesReadings = result;
            }, error => console.error(error));
        });
    }
}

export interface DiagnosticsDataDevicesReadings {
  diagnosticsDataDevicesReadings: DiagnosticsDataDeviceReading[];
}


export interface DiagnosticsDataDeviceReading {
  id: string,
  createdOn: number,
}
