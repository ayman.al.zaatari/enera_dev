import { Component, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHandler } from '@angular/common/http';

@Component({
  selector: 'app-reading-history',
  templateUrl: './reading-history.component.html'
})

export class ReadingComponent {
  public readingHistory: ReadingHistoryDto;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private router: ActivatedRoute) {
    this.router.params.subscribe(x => {
      http.get<ReadingHistoryDto>('api/reading/' + x.idSite + '/readings').subscribe(result => {
        console.log(result);
        this.readingHistory = result;
      }, error => console.error(error));
    });
  }
}


export interface ReadingHistoryDto {
  readings: ReadingHistory[];
}

export interface ReadingHistory {
  id: string[];
  createdOn: Date[];
}
