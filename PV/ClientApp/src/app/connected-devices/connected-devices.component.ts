import { Component, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHandler } from '@angular/common/http';

@Component({
    selector: 'app-connected-devices',
    templateUrl: './connected-devices.component.html'
})

export class ConnectedDevicesComponent {
    public connectedDevices: ConnectedDevices;

    constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private router: ActivatedRoute) {
        this.router.params.subscribe(x => {
            http.get<ConnectedDevices>(baseUrl + 'api/supplierintegration/' + x.idSite + '/devices').subscribe(result => {
                console.log(result);
                this.connectedDevices = result;
            }, error => console.error(error));
        });
    }
}

export interface ConnectedDevices {

    sucess: boolean;
    records: Records;
}

export interface Records {

    devices: Device[];
    unconfiguredDevices: boolean;
}

export interface Device {

    name: string;
    productCode: string;
    productName: string;
    firmwareVersion: string;
    lastConnection: number;
    class: string;
    loggingInterval: number;
    lastPowerUpOrRestart: number;
    instance: number;
}
