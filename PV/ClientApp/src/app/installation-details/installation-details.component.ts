import { Component, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Installation } from '../installations-list/installations-list.component';
import { HttpClient, HttpHandler } from '@angular/common/http';

@Component({
    selector: 'app-installation-details',
    templateUrl: './installation-details.component.html'
})

export class InstallationDetailsComponent {
    public panelDetail: Installation;
 

    constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private router: ActivatedRoute) {
        this.router.params.subscribe(x => {
            http.get<Installation>(baseUrl + 'api/supplierintegration/' + x.idSite).subscribe(result => {
                console.log(result);
                this.panelDetail = result;
            }, error => console.error(error));
        });
    }

    
}
