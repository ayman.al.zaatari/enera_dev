using System;
using System.Threading;
using AutoMapper;
using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PV.Business.Abstractions.IEngines;
using PV.Business.Abstractions.IEngines.Base;
using PV.Business.Abstractions.IEngines.Victron;
using PV.Business.DTO;
using PV.Business.DTO.IntegrationResponseDTO;
using PV.Business.DTO.IntegrationResponseDTO.Victron;
using PV.Business.Settings.Victron;
using PV.Core.Models;
using PV.DataAccess.Abstractions.IRepositories;
using PV.DataAccess.Abstractions.IUnitOfWork;
using PV.DataAccess.Context;
using PV.DataAccess.Repositories;
using PV.DataAccess.UnitOfWork;
using PV.Service.Business.Astractions.IServices;
using PV.Service.Business.Engines.Victron;
using PV.Service.Business.Jobs;
using PV.Service.Business.Services;
using PV.WEB.Middlewares;

namespace PV
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            JobStorage.Current = new SqlServerStorage(Configuration.GetConnectionString("HangfireConnection"));

            // Add Hangfire services.
            services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage(Configuration.GetConnectionString("HangfireConnection"), new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true
                }));

            // Add the processing server as IHostedService
            services.AddHangfireServer();

            services.AddControllersWithViews();
            // In production, the Angular files will be served from this directory

            services.AddAutoMapper(typeof(Startup));

            string options = Configuration.GetConnectionString("DefaultConnection");

            services.AddDbContext<DatabaseContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            RegisterSettings(services);
            RegisterRepositories(services);
            RegisterEngines(services);
            RegisterServices(services);
            RegisterJobs(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IBackgroundJobClient backgroundJobs, IWebHostEnvironment env)
        {
            app.UseMiddleware<ExceptionMiddleware>();
            //app.UseAuthentication();
            //app.UseAuthorization();

            UpdateDatabase(app);

            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/Error");
            //    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            //    app.UseHsts();
            //}

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseHangfireDashboard();

            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });

            app.UseHangfireDashboard();
            backgroundJobs.Enqueue(() => Console.WriteLine("Hello world from Hangfire!"));
        }

        private static void RegisterRepositories(IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IRepository<BaseModel>, Repository<BaseModel>>();
            services.AddTransient<ISiteRepository, SiteRepository>();
            services.AddTransient<IAppSettingsRepository, AppSettingsRepository>();
            services.AddTransient<ISupplierRepository, SupplierRepository>();
            services.AddTransient<IDiagnosticsDataRepository, DiagnosticsDataRepository>();
            services.AddTransient<IReadingRepository, ReadingRepository>();
            services.AddTransient<ICsvReadingRepository, CsvReadingRepository>();
            services.AddTransient<IDiagnosticsDataEnumValueRepository, DiagnosticsDataEnumValueRepository>();
            services.AddTransient<IKwhReadingRepository, KwhReadingRepository>();
        }

        private static void RegisterEngines(IServiceCollection services)
        {
            services.AddTransient<IBaseIntegrationEngine<VictronSettings, VictronLoginResponseIntegrationDTO, VictronInstallationsResponseIntegrationDTO, VictronInstallationResponseIntegrationDTO, VictronConnectedDevicesIntegrationDTO, VictronDiagnosticsDataIntegrationDTO, DiagnosticsData, VictronReadingKwhDTO>, VictronIntegrationEngine>();
            services.AddTransient<ISupplierCRUDEngine<SupplierResponseDTO, SupplierRequestDTO, SupplierUpdateRequestDTO>, VictronCRUDEngine>();
            services.AddTransient<IReadingEngine, ReadingEngine>();
            services.AddTransient<IReadingDetailsEngine, ReadingDetailsEngine>();
            services.AddTransient<IVictronIntegrationEngine, VictronIntegrationEngine>();
        }

        private static void RegisterServices(IServiceCollection services)
        {
            services.AddTransient<IBaseIntegrationService<VictronSettings, VictronLoginResponseIntegrationDTO, VictronInstallationsResponseIntegrationDTO, VictronInstallationResponseIntegrationDTO, VictronConnectedDevicesIntegrationDTO, VictronDiagnosticsDataIntegrationDTO, VictronReadingKwhDTO>, VictronIntegrationService>();
        }

        private void RegisterSettings(IServiceCollection services)
        {
            services.Configure<VictronSettings>(Configuration.GetSection("VictronSettings"));
        }

        private void RegisterJobs(IServiceCollection services)
        {
            RecurringJob.AddOrUpdate<GetDiagnosticsDataJob>(x => x.ExecuteAsync(new CancellationToken()), "*/5 * * * *");
            RecurringJob.AddOrUpdate<GetCsvReadingsJob>(x => x.ExecuteAsync(new CancellationToken()), "*/5 * * * *");
            RecurringJob.AddOrUpdate<GetKwhReadingsJob>(x => x.ExecuteAsync(new CancellationToken()), "*/5 * * * *");
        }

        private static void UpdateDatabase(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope();
            using var context = serviceScope.ServiceProvider.GetService<DatabaseContext>();
            context.Database.Migrate();
        }
    }
}