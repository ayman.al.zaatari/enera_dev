﻿using AutoMapper;
using PV.Business.DTO;
using PV.Business.DTO.IntegrationResponseDTO;
using PV.Core.Models;
using PV.Service.Business.Helpers;

namespace PV.WEB.Configuration.Automapper
{
    public class MapProfiles : Profile
    {
        public MapProfiles()
        {
            CreateMap<Supplier, SupplierResponseDTO>();

            CreateMap<SupplierRequestDTO, Supplier>();

            CreateMap<SupplierUpdateRequestDTO, Supplier>();

            CreateMap<VictronDiagnosticDataRecord, DiagnosticsData>()
                .ForMember(x => x.RequestId, y => y.MapFrom(z => z.Id))
                .ForMember(y => y.Timestamp, x => x.MapFrom(z => DateHelper.UnixTimeStampToDateTime(z.Timestamp)))
                .ForMember(z => z.Id, opt => opt.Ignore());

            CreateMap<VictronDataAttributeEnumValue, DiagnosticsDataEnumValue>();

            CreateMap<Reading, ReadingDTO>();

            CreateMap<DiagnosticsData, DiagnosticDataDTO>();
        }
    }
}
