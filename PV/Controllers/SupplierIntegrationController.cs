﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PV.Business.Abstractions.IEngines.Base;
using PV.Business.Abstractions.IEngines.Victron;
using PV.Business.DTO.IntegrationResponseDTO;
using PV.Business.DTO.IntegrationResponseDTO.Victron;
using PV.Business.Settings.Victron;
using PV.Core.Models;

namespace PV.Controllers
{

    public class SupplierIntegrationController : BaseController
    {
        public IBaseIntegrationEngine<VictronSettings, VictronLoginResponseIntegrationDTO, VictronInstallationsResponseIntegrationDTO, VictronInstallationResponseIntegrationDTO, VictronConnectedDevicesIntegrationDTO, VictronDiagnosticsDataIntegrationDTO, DiagnosticsData, VictronReadingKwhDTO> _supplierIntegrationEngine { get; set; }
        public IVictronIntegrationEngine _victronIntegrationEngine { get; set; }
        public SupplierIntegrationController(IBaseIntegrationEngine<VictronSettings, VictronLoginResponseIntegrationDTO, VictronInstallationsResponseIntegrationDTO, VictronInstallationResponseIntegrationDTO, VictronConnectedDevicesIntegrationDTO, VictronDiagnosticsDataIntegrationDTO, DiagnosticsData, VictronReadingKwhDTO> supplierIntegrationEngine,
            IVictronIntegrationEngine victronIntegrationEngine,
            ILogger<SupplierIntegrationController> logger) : base(logger)
        {
            _supplierIntegrationEngine = supplierIntegrationEngine;
            _victronIntegrationEngine = victronIntegrationEngine;
        }

        [HttpGet]
        [Route("{SupplierId}/Login")]
        public async Task<IActionResult> AuthenticateSupplierAsync(Guid SupplierId, CancellationToken ct)
        {
            var result = await _supplierIntegrationEngine.AuthenticateSupplierAsync(SupplierId, ct);

            return Ok(result);
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetAllInstallationsAsync(CancellationToken ct)
        {
            var result = await _supplierIntegrationEngine.GetAllInstallationsAsync(ct);

            return Ok(result.Records);
        }

        [HttpGet]
        [Route("{idSite}")]
        public async Task<IActionResult> GetSiteInstallations(int idSite, CancellationToken ct)
        {
            var result = await _supplierIntegrationEngine.GetInstallationAsync(idSite, ct);

            return Ok(result);
        }


        [HttpGet]
        [Route("{idSite}/devices")]
        public async Task<IActionResult> GetSiteConnectedDevices(int idSite, CancellationToken ct)
        {
            var result = await _supplierIntegrationEngine.GetSiteConnectedDevicesAsync(idSite, ct);

            return Ok(result);
        }

        [HttpGet]
        [Route("{idSite}/diagnostics")]
        public async Task<IActionResult> GetDiagnosticsDataDevices(int idSite, [FromQuery]int count, CancellationToken ct)
        {
            count = int.MaxValue;

            var result = await _supplierIntegrationEngine.GetDiagnosticsDataAsync(idSite, count, ct);

            return Ok(result.Records);
        }

        [HttpGet]
        [Route("csvreading")]
        public async Task<IActionResult> GetDiagnosticsDataDevices(int idSite, CancellationToken ct)
        {
            await _victronIntegrationEngine.DownloadInstallationCSV(ct);

            return Ok();
        }
    }
}
