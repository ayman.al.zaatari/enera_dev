﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PV.Business.Abstractions.IEngines;

namespace PV.Controllers
{

    public class ReadingController : BaseController
    {
        public IReadingEngine _readingEngine { get; set; }
        public IReadingDetailsEngine _readingDetailsEngine { get; set; }

        public ReadingController(IReadingEngine readingEngine, IReadingDetailsEngine readingDetailsEngine, ILogger<ReadingController> logger) : base(logger)
        {
            _readingDetailsEngine = readingDetailsEngine;
            _readingEngine = readingEngine;
        }

        [HttpGet]
        [Route("{idSite}/readings")]
        public async Task<IActionResult> GetReadingsBySiteAsync(int idSite, CancellationToken ct)
        {
            var result = await _readingEngine.GetReadingsBySiteAsync(idSite, int.MaxValue, ct);

            return Ok(result);
        }

        [HttpGet]
        [Route("{idReading}/diagnosticsdata")]
        public async Task<IActionResult> GetDiagnosticsReadingDetails(Guid idReading, CancellationToken ct)
        {
            var result = await _readingEngine.GetDiagnosticsDataByReadingAsync(idReading, ct);

            return Ok(result);
        }
    }
}
