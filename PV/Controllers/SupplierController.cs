﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PV.Business.Abstractions.IEngines;
using PV.Business.DTO;

namespace PV.Controllers
{

    public class SupplierController : BaseController
    {
        public ISupplierCRUDEngine<SupplierResponseDTO, SupplierRequestDTO, SupplierUpdateRequestDTO> _victronEngine { get; set; }

        public SupplierController(ISupplierCRUDEngine<SupplierResponseDTO, SupplierRequestDTO, SupplierUpdateRequestDTO> supplierLoginEngine,
            ILogger<SupplierController> logger) : base(logger)
        {
            _victronEngine = supplierLoginEngine;
        }
        
        [HttpGet]
        [Route("{SupplierId}")]
        public async Task<IActionResult> GetSupplier(Guid SupplierId, CancellationToken ct)
        {
            var result = await _victronEngine.GetAsync(SupplierId, ct);

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddSupplier(SupplierRequestDTO supplierRequestDTO, CancellationToken ct)
        {
            var result = await _victronEngine.CreateAsync(supplierRequestDTO, ct);

            return Ok(result);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateSupplier(SupplierUpdateRequestDTO supplierRequestDTO, CancellationToken ct)
        {
            var result = await _victronEngine.UpdateAsync(supplierRequestDTO, ct);

            return Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteSupplier(Guid supplierId, CancellationToken ct)
        {
            await _victronEngine.DeleteAsync(supplierId, ct);

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllSuppliers(CancellationToken ct)
        {
            var result = await _victronEngine.GetAllAsync(ct);

            return Ok(result);
        }
    }
}
